package test;

import java.io.IOException;
import java.util.Date;
import java.util.Random;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.jupiter.api.Test;

import model.specification.*;
import model.Joueur;
import model.Partie;

class CarteBienPlaceTest {

	@Before
	public Partie initialisation() throws IOException {
		Partie partie = new Partie();
		partie.setTypeJeu(typeJeu.cardline);
		partie.setThemeJeu(themeJeu.superficie);
		partie.lecture();
		Random  rnd;
		Date    dt;
		long    ms;

		
		for (int i = 0; i < 3; i++) {
			rnd = new Random();
			ms = -946771200000L + (Math.abs(rnd.nextLong()) % (70L * 365 * 24 * 60 * 60 * 1000));
			dt = new Date(ms);
			partie.addJoueur(new Joueur("joueur_" + i, dt, null));
		}
		partie.distribCartes();
		partie.trie();
		partie.premiereCarte();
		return partie;
	}

	@Test
	void BienPlaceTest() throws IOException {
		//le joueur insert la carte au bon endroit 
		Partie partie = initialisation();
		if(partie.getJoueurs().get(0).getMain().get(0).getValeur().compareTo(partie.getTapis().get(0).getValeur())<=0) {
			partie.ajoutCarte(0, 0, partie.getJoueurs().get(0).getMain().get(0));
			partie.testPlacement(0, partie.getJoueurs().get(0).getMain().get(0),0);
			Assertions.assertThat(partie.getTapis().size()).isEqualTo(2);
			Assertions.assertThat(partie.getJoueurs().get(0).getMain().size()).isEqualTo(5);
		}else {
			partie.ajoutCarte(0, 1, partie.getJoueurs().get(0).getMain().get(0));
			partie.testPlacement(0, partie.getJoueurs().get(0).getMain().get(0),0);
			Assertions.assertThat(partie.getTapis().size()).isEqualTo(2);
			Assertions.assertThat(partie.getJoueurs().get(0).getMain().size()).isEqualTo(5);
		}
	}
	
	@Test
	void MalPlaceTest() throws IOException {
		//le joueur insert la carte au mauvais endroit 
		Partie partie = initialisation();
		if(partie.getJoueurs().get(0).getMain().get(0).getValeur().compareTo(partie.getTapis().get(0).getValeur())>0) {
			partie.ajoutCarte(0, 0, partie.getJoueurs().get(0).getMain().get(0));
			partie.testPlacement(0, partie.getJoueurs().get(0).getMain().get(0),0);
			Assertions.assertThat(partie.getTapis().size()).isEqualTo(1);
			Assertions.assertThat(partie.getJoueurs().get(0).getMain().size()).isEqualTo(6);
		}else {
			partie.ajoutCarte(0, 1, partie.getJoueurs().get(0).getMain().get(0));
			partie.testPlacement(0, partie.getJoueurs().get(0).getMain().get(0),0);
			Assertions.assertThat(partie.getTapis().size()).isEqualTo(1);
			Assertions.assertThat(partie.getJoueurs().get(0).getMain().size()).isEqualTo(6);
		}
	}
}
