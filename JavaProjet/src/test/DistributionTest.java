package test;

import java.io.IOException;
import java.util.Date;
import java.util.Random;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.jupiter.api.Test;

import model.specification.*;
import model.Joueur;
import model.Partie;

class DistributionTest {

	Random  rnd;
	Date    dt;
	long    ms;
	@Before
	public Partie initialisation(int nbJoueurs) throws IOException {
		Partie partie = new Partie();
		partie.setTypeJeu(typeJeu.cardline);
		partie.setThemeJeu(themeJeu.superficie);
		partie.lecture();
		for (int i = 0; i < nbJoueurs; i++) {
			rnd = new Random();
			ms = -946771200000L + (Math.abs(rnd.nextLong()) % (70L * 365 * 24 * 60 * 60 * 1000));
			dt = new Date(ms);
			partie.addJoueur(new Joueur("joueur_" + i, dt, null));
		}
		partie.distribCartes();
		return partie;
	}

	@Test
	void testNbrJoueur2() throws IOException {
		Partie partie = initialisation(2);
		Assertions.assertThat(partie.getJoueurs().get(0).getMain().size()).isEqualTo(6);
		Assertions.assertThat(partie.getCarteJeu().size()).isEqualTo(48);
	}
	@Test
	void testNbrJoueur3() throws IOException {
		Partie partie = initialisation(3);
		Assertions.assertThat(partie.getJoueurs().get(0).getMain().size()).isEqualTo(6);
		Assertions.assertThat(partie.getCarteJeu().size()).isEqualTo(42);
	}
	@Test
	void testNbrJoueur4() throws IOException {
		Partie partie = initialisation(4);
		Assertions.assertThat(partie.getJoueurs().get(0).getMain().size()).isEqualTo(5);
		Assertions.assertThat(partie.getCarteJeu().size()).isEqualTo(40);
	}
	@Test
	void testNbrJoueur5() throws IOException {
		Partie partie = initialisation(5);
		Assertions.assertThat(partie.getJoueurs().get(0).getMain().size()).isEqualTo(5);
		Assertions.assertThat(partie.getCarteJeu().size()).isEqualTo(35);
	}
	@Test
	void testNbrJoueur6() throws IOException {
		Partie partie = initialisation(6);
		Assertions.assertThat(partie.getJoueurs().get(0).getMain().size()).isEqualTo(4);
		Assertions.assertThat(partie.getCarteJeu().size()).isEqualTo(36);
	}
	@Test
	void testNbrJoueur7() throws IOException {
		Partie partie = initialisation(7);
		Assertions.assertThat(partie.getJoueurs().get(0).getMain().size()).isEqualTo(4);
		Assertions.assertThat(partie.getCarteJeu().size()).isEqualTo(32);
	}
	@Test
	void testNbrJoueur8() throws IOException {
		Partie partie = initialisation(8);
		Assertions.assertThat(partie.getJoueurs().get(0).getMain().size()).isEqualTo(4);
		Assertions.assertThat(partie.getCarteJeu().size()).isEqualTo(28);
	}
}