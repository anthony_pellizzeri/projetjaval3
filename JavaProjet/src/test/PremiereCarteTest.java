package test;

import java.io.IOException;
import java.util.Date;
import java.util.Random;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import model.specification.*;
import model.Joueur;
import model.Partie;

class PremiereCarteTest {

	@Test
	void PremiereCarte() throws IOException {
		Random  rnd;
		Date    dt;
		long    ms;
		Partie partie = new Partie();
		partie.setTypeJeu(typeJeu.cardline);
		partie.setThemeJeu(themeJeu.superficie);
		partie.lecture();
		for (int i = 0; i < 3; i++) {
			rnd = new Random();
			ms = -946771200000L + (Math.abs(rnd.nextLong()) % (70L * 365 * 24 * 60 * 60 * 1000));
			dt = new Date(ms);
			partie.addJoueur(new Joueur("joueur_" + i, dt, null));
		}
		partie.distribCartes();
		partie.premiereCarte();
		Assertions.assertThat(partie.getCarteJeu().size()).isEqualTo(41);
	}

}
