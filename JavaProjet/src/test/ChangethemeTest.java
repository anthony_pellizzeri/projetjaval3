package test;

import java.io.IOException;
import java.util.Date;
import java.util.Random;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.jupiter.api.Test;

import model.specification.*;
import model.Joueur;
import model.Partie;
import model.cardLine;
import model.timeLine;

class ChangethemeTest {
	Random  rnd;
	Date    dt;
	long    ms;

	@Before
	public Partie initialisation() throws IOException {
		Partie partie = new Partie();
		partie.setTypeJeu(typeJeu.timeline);
		for (int i = 0; i < 3; i++) {
			rnd = new Random();
			ms = -946771200000L + (Math.abs(rnd.nextLong()) % (70L * 365 * 24 * 60 * 60 * 1000));
			dt = new Date(ms);
			partie.addJoueur(new Joueur("joueur_" + i, dt, null));
		}
		partie.lecture();
		return partie;
	}
	
	@Test
	void cardLinePIB() throws IOException {
		Partie partie = initialisation();
		partie.changeTheme("cardline","pib");		
		Assertions.assertThat(partie.getCarteJeu().get(0).getValeur()).isEqualTo(((cardLine) partie.getCarteJeu().get(0)).getPib());
		Assertions.assertThat(partie.getJoueurs().get(0).getMain()).isEqualTo(null);
		Assertions.assertThat(partie.getTapis().size()).isEqualTo(0);
	}
	
	@Test
	void cardLinePollution() throws IOException {
		Partie partie = initialisation();
		partie.changeTheme("cardline","pollution");		
		Assertions.assertThat(partie.getCarteJeu().get(0).getValeur()).isEqualTo(((cardLine) partie.getCarteJeu().get(0)).getPollution());
		Assertions.assertThat(partie.getJoueurs().get(0).getMain()).isEqualTo(null);
		Assertions.assertThat(partie.getTapis().size()).isEqualTo(0);
	}
//	
	@Test
	void cardLinePopulation() throws IOException {
		Partie partie = initialisation();
		partie.changeTheme("cardline","population");		
		Assertions.assertThat(partie.getCarteJeu().get(0).getValeur()).isEqualTo(((cardLine) partie.getCarteJeu().get(0)).getPopulation());
		Assertions.assertThat(partie.getJoueurs().get(0).getMain()).isEqualTo(null);
		Assertions.assertThat(partie.getTapis().size()).isEqualTo(0);
	}
//	
	@Test
	void cardLineSuperficie() throws IOException {
		Partie partie = initialisation();
		partie.changeTheme("cardline","superficie");		
		Assertions.assertThat(partie.getCarteJeu().get(0).getValeur()).isEqualTo(((cardLine) partie.getCarteJeu().get(0)).getSuperficie());
		Assertions.assertThat(partie.getJoueurs().get(0).getMain()).isEqualTo(null);
		Assertions.assertThat(partie.getTapis().size()).isEqualTo(0);
	}
	@Test
	void timeline() throws IOException {
		Partie partie = initialisation();
		partie.changeTheme("timeline",null);		
		Assertions.assertThat(partie.getCarteJeu().get(0).getValeur()).isEqualTo(((timeLine) partie.getCarteJeu().get(0)).getDate());
		Assertions.assertThat(partie.getJoueurs().get(0).getMain()).isEqualTo(null);
		Assertions.assertThat(partie.getTapis().size()).isEqualTo(0);
	}

}
