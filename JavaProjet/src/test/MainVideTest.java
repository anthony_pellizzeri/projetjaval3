package test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import model.specification.*;
import model.Joueur;
import model.Partie;

class MainVideTest {

	@Test
	void TestMainVide() throws IOException {
		Random  rnd;
		Date    dt;
		long    ms;
		Partie partie = new Partie();
		partie.setTypeJeu(typeJeu.cardline);
		partie.setThemeJeu(themeJeu.superficie);
		for (int i = 0; i < 3; i++) {
			rnd = new Random();
			ms = -946771200000L + (Math.abs(rnd.nextLong()) % (70L * 365 * 24 * 60 * 60 * 1000));
			dt = new Date(ms);
			partie.addJoueur(new Joueur("joueur_" + i, dt, null));
		}
		partie.lecture();
		partie.distribCartes();
		partie.getJoueurs().get(1).getMain().clear();
		ArrayList<Joueur> gagnant = new ArrayList<Joueur>();
		for (int n = 0; n < partie.getJoueurs().size(); n++) {
			if(partie.mainVide(n, gagnant)) {
				n--;
			};
		}
		Assertions.assertThat(gagnant.size()).isEqualTo(1);
	}

}
