package test;

import java.io.IOException;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import model.specification.*;
import model.Partie;

class LectureTest {

	@Test
	void testPartieTimeLine() throws IOException {
		Partie partie = new Partie();
		partie.setTypeJeu(typeJeu.timeline);
		partie.setThemeJeu(null);
		partie.lecture();
		Assertions.assertThat(partie.getCarteJeu().size()).isEqualTo(60);
	}
	@Test
	void testPartieCardLineSuperficie() throws IOException {
		Partie partie = new Partie();
		partie.setTypeJeu(typeJeu.cardline);
		partie.setThemeJeu(themeJeu.superficie);
		partie.lecture();
		Assertions.assertThat(partie.getCarteJeu().size()).isEqualTo(60);
	}
	@Test
	void testPartieCardLinePIB() throws IOException {
		Partie partie = new Partie();
		partie.setTypeJeu(typeJeu.cardline);
		partie.setThemeJeu(themeJeu.pib);
		partie.lecture();
		Assertions.assertThat(partie.getCarteJeu().size()).isEqualTo(60);
	}
	@Test
	void testPartieCardLinePollution() throws IOException {
		Partie partie = new Partie();
		partie.setTypeJeu(typeJeu.cardline);
		partie.setThemeJeu(themeJeu.pollution);
		partie.lecture();
		Assertions.assertThat(partie.getCarteJeu().size()).isEqualTo(60);
	}
	@Test
	void testPartieCardLinePopulation() throws IOException {
		Partie partie = new Partie();
		partie.setTypeJeu(typeJeu.cardline);
		partie.setThemeJeu(themeJeu.population);
		partie.lecture();
		Assertions.assertThat(partie.getCarteJeu().size()).isEqualTo(60);
	}

}
