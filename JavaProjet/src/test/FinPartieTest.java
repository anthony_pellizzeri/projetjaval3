package test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.jupiter.api.Test;

import model.specification.*;
import model.Carte;
import model.Joueur;
import model.Partie;

class FinPartieTest {@Before
	public Partie initialisation() throws IOException {
	Random  rnd;
	Date    dt;
	long    ms;
	Partie partie = new Partie();
	partie.setTypeJeu(typeJeu.cardline);
	partie.setThemeJeu(themeJeu.superficie);
	for (int i = 0; i < 5; i++) {
		rnd = new Random();
		ms = -946771200000L + (Math.abs(rnd.nextLong()) % (70L * 365 * 24 * 60 * 60 * 1000));
		dt = new Date(ms);
		partie.addJoueur(new Joueur("joueur_" + i, dt, null));
	}
	partie.lecture();
	partie.distribCartes();
	return partie;
}
	
	

	@Test
	void TestPartieContinueAvecPlusieursGagnants() throws IOException {
		Partie partie = initialisation();
		partie.getJoueurs().get(2).getMain().clear();
		partie.getJoueurs().get(3).getMain().clear();
		ArrayList<Joueur> gagnant = new ArrayList<Joueur>();
		for (int n = 0; n < partie.getJoueurs().size(); n++) {
			if(partie.mainVide(n, gagnant)) {
				n--;
			}
			partie.testFinPartie(n, gagnant);
		}
		partie.partieFinale(gagnant);
		Assertions.assertThat(partie.getJoueurs().size()).isEqualTo(2);
		Assertions.assertThat(partie.getJoueurs().get(0).getMain().size()).isEqualTo(1);
	}
	
	@SuppressWarnings("unchecked")
	@Test
	void TestPartieFinieUnGagnant() throws IOException {
		Partie partie = initialisation();
		partie.getCarteUtilise().addAll((ArrayList<Carte>) partie.getJoueurs().get(2).getMain().clone());
		partie.getJoueurs().get(2).getMain().clear();
		ArrayList<Joueur> gagnant = new ArrayList<Joueur>();
		for (int n = 0; n < partie.getJoueurs().size(); n++) {
			if(partie.mainVide(n, gagnant)) {
				n--;
			}
			partie.testFinPartie(n, gagnant);
		}
		
		partie.partieFinale(gagnant);
		Assertions.assertThat(partie.getJoueurs().get(0).getMain().size()).isEqualTo(0);
		Assertions.assertThat(partie.getJoueurs().get(0).getPoint()).isEqualTo(1);
		
//		au moins 3 joueur donc 2 perdants
		Assertions.assertThat(partie.getPerdants().size()).isGreaterThanOrEqualTo(2);
		
		
		
		partie.recommencerJeu();
		Assertions.assertThat(partie.getJoueurs().size()).isEqualTo(5);
		Assertions.assertThat(partie.getCarteJeu().size()).isEqualTo(60);
		Assertions.assertThat(partie.getTapis().size()).isEqualTo(0);
		Assertions.assertThat(partie.getPerdants().size()).isEqualTo(0);
		Assertions.assertThat(partie.getCarteUtilise().size()).isEqualTo(0);


	}

}
