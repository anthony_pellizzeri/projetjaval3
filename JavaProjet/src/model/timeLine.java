package model;

/**
 * 
 * carte timeline
 *
 */
public class timeLine extends Carte{

	private static final long serialVersionUID = 5300661059038562590L;
	private Double date;

	/**
	 * 
	 * @param illustration
	 * @param valeur
	 * @param imgFaceOuverte
	 * @param imgFaceCache
	 * @param date
	 */
	public timeLine(String illustration, Double valeur, String imgFaceOuverte, String imgFaceCache, Double date) {
		super(illustration, valeur, imgFaceOuverte, imgFaceCache);
		this.date = date;
	}

	public Double getDate() {
		return date;
	}

	public void setDate(Double date) {
		this.date = date;
	}

	@Override
	public String toString() {
		return "timeLine [date=" + date + "]";
	}
}
