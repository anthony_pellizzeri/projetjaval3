package model;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.StringTokenizer;

import model.specification.themeJeu;
import model.specification.typeJeu;
/**
 * 
 * partie
 *
 */
public class Partie implements Serializable {
	

	private static final long serialVersionUID = -6471253774802540372L;
	private ArrayList<Carte> tapis = new ArrayList<Carte>();
	private ArrayList<Joueur> joueurs = new ArrayList<Joueur>();
	private ArrayList<Joueur> perdants = new ArrayList<Joueur>();
	private ArrayList<Carte> carteJeu = new ArrayList<Carte>();
	private ArrayList<Carte> carteUtilise = new ArrayList<Carte>();
	private typeJeu typeJeu;
	private themeJeu themeJeu;

	public themeJeu getThemeJeu() {
		return themeJeu;
	}

	public void setThemeJeu(themeJeu themeJeu) {
		this.themeJeu = themeJeu;
	}
	/**
	 * 
	 * @param jeu
	 */
	public Partie(Partie jeu) {
		this.tapis=jeu.tapis;
		this.joueurs=jeu.joueurs;
		this.perdants=jeu.perdants;
		this.carteJeu=jeu.carteJeu;
		this.carteUtilise=jeu.carteUtilise;
		this.typeJeu=jeu.typeJeu;
		this.themeJeu=jeu.themeJeu;
	}

	public Partie() { 
	}
	/**
	 * lire le CSV en fonction des enum typejeu et themejeu et créer les cartes 
	 * @throws IOException
	 */
	@SuppressWarnings({ "unused", "static-access" })
	public void lecture() throws IOException {
		String chemin = "data/" + typeJeu.toString() + "/" + typeJeu.toString() + ".csv";
		int i = 0;
		File file = new File(chemin);
		FileReader fr = new FileReader(file);
		BufferedReader br = new BufferedReader(fr);
		StringTokenizer st = null;
		br.readLine();
		String img;
		for (String line = br.readLine(); line != null; line = br.readLine()) {
			st = new StringTokenizer(line, ";");
			String illustration = st.nextToken();
			if (typeJeu == typeJeu.cardline) {
				Double supperficie = Double.parseDouble(st.nextToken());
				Double popilation = Double.parseDouble(st.nextToken());
				Double pib = Double.parseDouble(st.nextToken());
				Double polution = Double.parseDouble(st.nextToken().replaceAll(",", "."));
				img = st.nextToken();
				Double themeCardLine = 0.0;
				switch (themeJeu.toString()) {
				case "superficie":
					themeCardLine = supperficie;
					break;
				case "population":
					themeCardLine = popilation;
					break;
				case "pib":
					themeCardLine = pib;
					break;
				case "pollution":
					themeCardLine = polution;
					break;
				}
				carteJeu.add(new cardLine(illustration, themeCardLine, img + "_reponse.jpeg", img + ".jpeg",
						supperficie, popilation, pib, polution));
			} else {
				String themeTimeLine = st.nextToken();
				Double valeur = Double.parseDouble(themeTimeLine);
				Double date = Double.parseDouble(themeTimeLine);
				img = st.nextToken();
				carteJeu.add(new timeLine(illustration, valeur, img + "_date.jpeg", img + ".jpeg", date));
			}
			i++;
		}
		br.close();
	}

	public ArrayList<Carte> getTapis() {
		return tapis;
	}

	public void setTapis(ArrayList<Carte> tapis) {
		this.tapis = tapis;
	}

	public ArrayList<Joueur> getJoueurs() {
		return joueurs;
	}

	public void setJoueurs(ArrayList<Joueur> joueurs) {
		this.joueurs = joueurs;
	}

	public ArrayList<Carte> getCarteJeu() {
		return carteJeu;
	}

	public void setCarteJeu(ArrayList<Carte> carteJeu) {
		this.carteJeu = carteJeu;
	}

	public typeJeu getTypejeu() {
		return typeJeu;
	}

	public void setTypeJeu(typeJeu typejeu) {
		this.typeJeu = typejeu;
	}
	
	/**
	 * distribuer les cartes aux participants en fonction du nombre de joueurs
	 */
	public void distribCartes() {
		int nbrCartes = 0;
		switch (this.joueurs.size()) {
		case 2:
		case 3:
			nbrCartes = 6;
			break;
		case 4:
		case 5:
			nbrCartes = 5;
			break;
		case 6:
		case 7:
		case 8:
			nbrCartes = 4;
			break;
		}
		int supp;
		for (int i = 0; i < this.joueurs.size(); i++) {
			ArrayList<Carte> cartes = new ArrayList<Carte>();
			for (int n = 0; n < nbrCartes; n++) {
				supp = (int) (Math.random() * (carteJeu.size()));
				cartes.add(carteJeu.get(supp));
				carteJeu.remove(supp);
			}
			joueurs.get(i).setMain(cartes);
		}
	}
	
	/**
	 * trie les joueurs en fonction de leurs age ou de leurs nombre de point
	 */
	public void trie() {
		// test si des joueurs ont des points
		boolean joueurAvecPoints = false;
		for (int i = 0; i < joueurs.size(); i++) {
			if (joueurs.get(i).getPoint() > 0) {
				joueurAvecPoints = true;
			}
		}
		// trie en fonction des points (plus grand au plus petit)
		if (joueurAvecPoints) {
			Collections.sort(joueurs, (joueur1, joueur2) -> joueur1.getPoint().compareTo(joueur2.getPoint()));
			Collections.reverse(joueurs);
		} else {
			// trie en fonction des ages (plus petit au plus grand)
			Collections.sort(joueurs, (joueur1, joueur2) -> joueur1.getAge().compareTo(joueur2.getAge()));
		}

	}

	
	/**
	 * test le bon positionnement de la carte sur le tapis par le joueur
	 * @param c
	 * @return true si bien place ou false si mal place
	 */
	public boolean bonneplace(Integer c) {
		if (tapis.size() == 2 && tapis.get(0).getValeur().compareTo(tapis.get(1).getValeur()) > 0) {
			tapis.remove(c.intValue());
			return false;
		} else if (tapis.size() > 2) {
			for (int i = 0; i < tapis.size()- 1; i++) {
				if (tapis.get(i).getValeur().compareTo(tapis.get(i + 1).getValeur()) > 0) {
					tapis.remove(c.intValue());
					return false;
				}
			}
		}

		return true;
	}

	/**
	 * si le positionnement est mal fait, ajoute une carte au joueur et place l'autre dans les utilisée
	 * @param joueur
	 * @param Utilise
	 * @param pos
	 * 
	 */
	public void testPlacement(int joueur, Carte Utilise, int pos) {
		if (!bonneplace(pos)) {
			carteUtilise.add(Utilise);
			if(carteJeu.size()==0) {
				carteJeu=(ArrayList<Carte>) carteUtilise;
				carteUtilise.clear();
			}
			int carteAleatoire = (int) (Math.random() * (carteJeu.size()));
			joueurs.get(joueur).getMain().add(carteJeu.get(carteAleatoire));
			carteJeu.remove(carteAleatoire);
		}
	}
	
	/**
	 * ajout carte a tout les joueurs
	 * @param joueur
	 * @param pos
	 * @param carte
	 * @return
	 */
	public Carte ajoutCarte(int joueur, int pos, Carte carte) {
		tapis.add(pos, carte);
		joueurs.get(joueur).getMain().remove(carte);
		return carte;

	}

	public ArrayList<Carte> getCarteUtilise() {
		return carteUtilise;
	}

	public void setCarteUtilise(ArrayList<Carte> carteUtilise) {
		this.carteUtilise = carteUtilise;
	}

	/**
	 * test si la main est vide pour savoir si le joueur va etre dans la liste des gagnants
	 * @param pos
	 * @param gagnant
	 * @return
	 */
	public Boolean mainVide(int pos, ArrayList<Joueur> gagnant) {
		if (joueurs.get(pos).getMain().isEmpty()) {
			gagnant.add(joueurs.get(pos));
			joueurs.remove(pos);
			return true;
		}
		return false;
	}

	/**
	 * test si il y a des gagnants et que le tour est fini
	 * @param pos
	 * @param gagnant
	 * @return
	 */
	public Boolean testFinPartie(int pos, ArrayList<Joueur> gagnant) {
		if (pos == (joueurs.size() - 1) && gagnant.size() > 0) {
			return true;
		}
		return false;
	}

	/**
	 * place une carte sur le tapis
	 */
	public void premiereCarte() {
		int pos = (int) (Math.random() * (carteJeu.size()));
		tapis.add(getCarteJeu().get(pos));
		carteJeu.remove(pos);
	}

	/**
	 * test si la partie est finie et qu'il y a 1 gagnant ou plusieurs donc recommencer la partie sauf si le nombre de carte est insufisant 
	 * @param gagnants
	 */
	@SuppressWarnings("unchecked")
	public void partieFinale(ArrayList<Joueur> gagnants) {
		// trier joueurs perdants en fct de leurs mains
		Collections.sort(this.joueurs,
				(joueur1, joueur2) -> Integer.compare(joueur1.getMain().size(), joueur2.getMain().size()));
		perdants.addAll(0, joueurs); // au debut pour garder le classement

		joueurs = (ArrayList<Joueur>) gagnants.clone();
		gagnants.clear();
		trie();
		// asigne 1 carte aléatoire aux joueurs restants(gagants) si nbr >1
		if (joueurs.size() > 1) {
			for (int i = 0; i < joueurs.size(); i++) {
				if(!(carteUtilise.size()==0 && carteJeu.size()==0)) {
					if(carteJeu.size()+carteUtilise.size()<joueurs.size() ) {
//						egalitePoint();
					}else {
						if(carteJeu.size()==0) {
							carteJeu=(ArrayList<Carte>) carteUtilise;
							carteUtilise.clear();
						}
						int aleatoire = (int) (Math.random() * (carteJeu.size()));
						joueurs.get(i).getMain().add(carteJeu.get(aleatoire));
						carteJeu.remove(aleatoire);
					}
					
				}
			}
		} else {
			joueurs.get(0).setPoint(joueurs.get(0).getPoint() + 1);
		}

	}

	public void addJoueur(Joueur j) {
		joueurs.add(j);
		// j.setPoint(0);
	}
	/**
	 * changer le theme du jeu ou le type de jeu
	 * une lecture sera faite seulement au passage d'un jeu à l'autre et nom à un changement de theme
	 * @param jeu
	 * @param theme
	 * @throws IOException
	 */
	@SuppressWarnings("static-access")
	public void changeTheme(String jeu, String theme) throws IOException {
		if (jeu == "cardline" && typeJeu == typeJeu.cardline) {

			
			// cartes utilisées 
						for(Carte c: this.tapis) {
							cardLine carte = (cardLine) c;
							switch (theme) {
								case "superficie":
								c.setValeur(carte.getSuperficie());
								break;
					
								case "population":
								c.setValeur(carte.getPopulation());
								break;
					
								case "pib":
								c.setValeur(carte.getPib());
								break;
					
								case "pollution":
								c.setValeur(carte.getPollution());
								break;
							}
						}
						// cartes utilisées 
						for(Carte c: this.carteJeu) {
							cardLine carte = (cardLine) c;
							switch (theme) {
								case "superficie":
								c.setValeur(carte.getSuperficie());
								break;
					
								case "population":
								c.setValeur(carte.getPopulation());
								break;
					
								case "pib":
								c.setValeur(carte.getPib());
								break;
					
								case "pollution":
								c.setValeur(carte.getPollution());
								break;
							}
						}
			// cartes utilisées 
			for(Carte c: this.carteUtilise) {
				cardLine carte = (cardLine) c;
				switch (theme) {
					case "superficie":
					c.setValeur(carte.getSuperficie());
					break;
		
					case "population":
					c.setValeur(carte.getPopulation());
					break;
		
					case "pib":
					c.setValeur(carte.getPib());
					break;
		
					case "pollution":
					c.setValeur(carte.getPollution());
					break;
				}
			}
			//pour chaque joueur
			for(Joueur j: this.joueurs) {
				for (Carte c : j.getMain()) {
					cardLine carte = (cardLine) c;
					switch (theme) {
						case "superficie":
						c.setValeur(carte.getSuperficie());
						break;
			
						case "population":
						c.setValeur(carte.getPopulation());
						break;
			
						case "pib":
						c.setValeur(carte.getPib());
						break;
			
						case "pollution":
						c.setValeur(carte.getPollution());
						break;
					}
				}
			}
			typeJeu = typeJeu.valueOf(jeu);
			themeJeu = themeJeu.valueOf(theme);
		} else if((typeJeu==typeJeu.timeline && jeu=="cardline") || (typeJeu==typeJeu.cardline && jeu=="timeline")) {
			carteJeu.clear();
			typeJeu = typeJeu.valueOf(jeu);
			if (jeu != "timeline") {
				themeJeu = themeJeu.valueOf(theme);
			}
			lecture();			
		}

	}
	
	/**
	 * recommencer une partie donc replacer toute les cartes dans le jeu
	 */
	@SuppressWarnings("unchecked")
	public void recommencerJeu() {
		
		for (int i = 0; i < perdants.size(); i++) {
			carteJeu.addAll((ArrayList<Carte>) perdants.get(i).getMain());
			perdants.get(i).getMain().clear();
		}
		
		
		joueurs.addAll((ArrayList<Joueur>) perdants.clone());
		perdants.clear();
		
		for (int i = 0; i < joueurs.size(); i++) {
			joueurs.get(i).getMain().clear();
			joueurs.get(i).setBonusAppel(true);
		}
		carteJeu.addAll((ArrayList<Carte>) carteUtilise);
		carteUtilise.clear();
		carteJeu.addAll((ArrayList<Carte>) tapis);
		tapis.clear();		
		trie();
	}

	public ArrayList<Joueur> getPerdants() {
		return perdants;
	}

	public void setPerdants(ArrayList<Joueur> perdants) {
		this.perdants = perdants;
	}
	
	public void egalitePoint() {
		for(int i=0;i<joueurs.size();i++) {
			joueurs.get(i).setPoint(joueurs.get(i).getPoint()+1);
		}
		
	}
	
	/**
	 * en fonction de l'ami suggestion faite sur une carte aleatoire du tapis
	 * @param posJoueur
	 * @param amiChoisi
	 * @param choixMain
	 * @return position du bouton suggeré par l'ami
	 */
	public int bonusAppel(int posJoueur, String amiChoisi, int choixMain) {
		int pourcentage=0;
		switch (amiChoisi) {
			case "Julien":
				pourcentage=0;
				break;
			case "Quentin":
				pourcentage=70;
				break;
			case "Anthony":
				pourcentage=80;
				break;
			}
		
		int numRand = (int) (Math.random() * (100));
		int posCarteTapis = 0;
		Integer bonnePos = null;
		
		// forcer à bien placer carte

		int i = 0;
		while (bonnePos == null) {
			if (tapis.get(i).getValeur() > joueurs.get(posJoueur).getMain().get(choixMain).getValeur()) {
				bonnePos = i;
			} else if (tapis.size() - 1 == i) {
				bonnePos = i + 1;
			} else {
				i++;
			}
		}


		if (numRand >= pourcentage) {
			// forcer à mal placer carte
			
			
			do {
				posCarteTapis = (int) (Math.random() * (tapis.size()+1));
				
			}while(posCarteTapis == bonnePos);
				bonnePos = posCarteTapis;
		}
		return bonnePos;

	}
	
	/**
	 * trie le tapis lors du changement de theme en cours de partie 
	 */
	public void trietapis() {
		Collections.sort(tapis, (carte1, carte2) -> carte1.getValeur().compareTo(carte2.getValeur()));
	}
	
	

}
