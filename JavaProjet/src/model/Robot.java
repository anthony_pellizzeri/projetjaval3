package model;

import java.io.Serializable;

import model.specification.NiveauRobot;

/**
 * 
 * robot avec niveau
 *
 */
public class Robot implements Serializable {
	private static final long serialVersionUID = -458134120662743760L;

	private Partie partieLocale;

	@SuppressWarnings("incomplete-switch")
	/**
	 * robot qui en fonction du niveau va chercher la bonne position sur le tapis d'une carte de sa main et va bien ou mal la placer
	 * @param partie
	 * @param posJoueur
	 */
	public Robot(Partie partie, int posJoueur) {
		super();
		this.partieLocale = partie;
		int pourcentage = 0;
		NiveauRobot niveau = partie.getJoueurs().get(posJoueur).getNiveauRobot();
		switch (niveau) {
		case facile:
			pourcentage = 30;
			break;
		case moyen:
			pourcentage = 50;
			break;
		case dificile:
			pourcentage = 80;
			break;
		}

		int numRand = (int) (Math.random() * (100));
		int choixMain = (int) (Math.random() * (partieLocale.getJoueurs().get(posJoueur).getMain().size()));
		int posCarteTapis = 0;
		Integer bonnePos = null;

		// forcer à bien placer carte

		int i = 0;
		while (bonnePos == null) {
			if (partieLocale.getTapis().get(i).getValeur() > partieLocale.getJoueurs().get(posJoueur).getMain().get(choixMain).getValeur()) {
				bonnePos = i;
			} else if (partieLocale.getTapis().size() - 1 == i) {
				bonnePos = i + 1;
			} else {
				i++;
			}
		}
		

		if (numRand >= pourcentage) {
			// forcer à mal placer carte

			do {
				posCarteTapis = (int) (Math.random() * (partie.getTapis().size()+1));
				
			}while(posCarteTapis == bonnePos);
			bonnePos = posCarteTapis;

		}

		Carte carteUtilise = new Carte(partie.getJoueurs().get(posJoueur).getMain().get(choixMain));
		partie.ajoutCarte(posJoueur, bonnePos, partie.getJoueurs().get(posJoueur).getMain().get(choixMain));
		partie.testPlacement(posJoueur, carteUtilise, bonnePos);
	}

}
