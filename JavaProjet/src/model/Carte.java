package model;

import java.io.Serializable;

public class Carte implements Serializable {

	private static final long serialVersionUID = -1439890082200359907L;
	private String illustration;
	private Double valeur;
	private String imgFaceOuverte;
	private String imgFaceCache;

	/**
	 * 
	 * @param illustration
	 * @param valeur
	 * @param imgFaceOuverte
	 * @param imgFaceCache
	 */
	
	public Carte(String illustration, Double valeur, String imgFaceOuverte, String imgFaceCache) {
		super();
		this.illustration = illustration;
		this.valeur = valeur;
		this.imgFaceOuverte = imgFaceOuverte;
		this.imgFaceCache = imgFaceCache;
	}
	/**
	 * 
	 * @param c
	 */
	public Carte(Carte c) {
		this.illustration = c.illustration;
		this.valeur = c.valeur;
		this.imgFaceOuverte = c.imgFaceOuverte;
		this.imgFaceCache = c.imgFaceCache;
	}

	public String getIllustration() {
		return illustration;
	}

	public void setIllustration(String illustration) {
		this.illustration = illustration;
	}

	public Double getValeur() {
		return valeur;
	}

	public void setValeur(Double valeur) {
		this.valeur = valeur;
	}

	public String getImgFaceOuverte() {
		return imgFaceOuverte;
	}

	public void setImgFaceOuverte(String imgFaceOuverte) {
		this.imgFaceOuverte = imgFaceOuverte;
	}

	public String getImgFaceCache() {
		return imgFaceCache;
	}

	public void setImgFaceCache(String imgFaceCache) {
		this.imgFaceCache = imgFaceCache;
	}

	@Override
	public String toString() {
		return "Carte [illustration=" + illustration + ", valeur=" + valeur + ", imgFaceOuverte=" + imgFaceOuverte
				+ ", imgFaceCache=" + imgFaceCache + "]";
	}
}
