package model;

/**
 * 
 * carte cardline
 *
 */
public class cardLine extends Carte{
	private static final long serialVersionUID = -2506971258973210190L;
	private double superficie;
	private double population;
	private double pib;
	private double pollution;
	
	/**
	 * 
	 * @param illustration
	 * @param valeur
	 * @param imgFaceOuverte
	 * @param imgFaceCache
	 * @param superficie
	 * @param population
	 * @param pib
	 * @param pollution
	 */
	public cardLine(String illustration, Double valeur, String imgFaceOuverte, String imgFaceCache, double superficie,
			double population, double pib, double pollution) {
		super(illustration, valeur, imgFaceOuverte, imgFaceCache);
		this.superficie = superficie;
		this.population = population;
		this.pib = pib;
		this.pollution = pollution;
	}

	public double getSuperficie() {
		return superficie;
	}

	public void setSuperficie(double superficie) {
		this.superficie = superficie;
	}

	public double getPopulation() {
		return population;
	}

	public void setPopulation(double population) {
		this.population = population;
	}

	public double getPib() {
		return pib;
	}

	public void setPib(double pib) {
		this.pib = pib;
	}

	public double getPollution() {
		return pollution;
	}

	public void setPollution(double pollution) {
		this.pollution = pollution;
	}

	@Override
	public String toString() {
		return "cardLine [superficie=" + superficie + ", population=" + population + ", pib=" + pib + ", pollution="
				+ pollution + "]";
	}
	
	
	
}
