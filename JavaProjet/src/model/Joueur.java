package model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

import model.specification.NiveauRobot;
/**
 * 
 * joueur
 *
 */
public class Joueur implements Serializable {
	private static final long serialVersionUID = -3348213447993060124L;
	private String pseudo;
	private Date age;
	private ArrayList<Carte> main = new ArrayList<Carte>();
	private Integer point;
	private NiveauRobot niveauRobot;
	private boolean BonusAppel = true;


	/**
	 * 
	 * @param pseudo
	 * @param age
	 * @param main
	 */
	public Joueur(String pseudo, Date age, ArrayList<Carte> main) {
		super();
		this.pseudo = pseudo;
		this.age = age;
		this.main = main;
		this.point = 0;
		this.niveauRobot = NiveauRobot.aucun;
	}

	public String getpseudo() {
		return pseudo;
	}

	public void setpseudo(String pseudo) {
		this.pseudo = pseudo;
	}

	public Date getAge() {
		return age;
	}

	public void setAge(Date age) {
		this.age = age;
	}

	public ArrayList<Carte> getMain() {
		return main;
	}

	public void setMain(ArrayList<Carte> main) {
		this.main = main;
	}

	@Override
	public String toString() {
		return "Joueur [pseudo=" + pseudo + ", age=" + age + ", main=" + main + "]";
	}

	public Integer getPoint() {
		return point;
	}

	public void setPoint(Integer point) {
		this.point = point;
	}

	public NiveauRobot getNiveauRobot() {
		return niveauRobot;
	}

	public void setNiveauRobot(NiveauRobot niveauRobot) {
		this.niveauRobot = niveauRobot;
	}
	
	public boolean isBonusAppel() {
		return BonusAppel;
	}

	public void setBonusAppel(boolean bonusAppel) {
		BonusAppel = bonusAppel;
	}

	public String getPseudo() {
		return pseudo;
	}

	public void setPseudo(String pseudo) {
		this.pseudo = pseudo;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
