package controleur;

import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import model.Partie;
import vue.VueChoixTheme;
import vue.VueJeu;

/**
 * 
 * gerer le choix du theme
 *
 */
public class ControleurChoixTheme {
	private Partie partieLocale;
	private VueChoixTheme vue;
	
	/**
	 * 
	 * @param partie
	 * @param vue
	 */
	public ControleurChoixTheme(Partie partie, VueChoixTheme vue) {
		super();
		this.partieLocale = partie;
		this.vue = vue;
		
		vue.getPib().addActionListener(new clickPIB());
		vue.getPollution().addActionListener(new clickPollution());
		vue.getPopulation().addActionListener(new clickPopulation());
		vue.getSuperficie().addActionListener(new clickSuperficie());
	}
	
	private class clickPIB implements ActionListener {
		@SuppressWarnings("unused")
		public void actionPerformed(ActionEvent e) {
			try {
				partieLocale.changeTheme("cardline","pib");
				System.out.println("type: "+partieLocale.getTypejeu().toString());
			} catch (IOException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}
			try {
				((Window) vue.getPane().getTopLevelAncestor()).dispose();
				ControleurJeu j=new ControleurJeu(new VueJeu(), partieLocale,null,null);
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} 
		}
	}
	
	class clickPopulation implements ActionListener {
		@SuppressWarnings("unused")
		public void actionPerformed(ActionEvent e) {
			try {
				partieLocale.changeTheme("cardline","population");
				System.out.println("type: "+partieLocale.getTypejeu().toString());
			} catch (IOException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}
			try {
				((Window) vue.getPane().getTopLevelAncestor()).dispose();
				ControleurJeu j=new ControleurJeu(new VueJeu(), partieLocale,null,null);
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} 
		}
	}
	
	class clickPollution implements ActionListener {
		@SuppressWarnings("unused")
		public void actionPerformed(ActionEvent e) {
			try {
				partieLocale.changeTheme("cardline","pollution");
				System.out.println("type: "+partieLocale.getTypejeu().toString());
			} catch (IOException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}
			try {
				((Window) vue.getPane().getTopLevelAncestor()).dispose();
				ControleurJeu j=new ControleurJeu(new VueJeu(), partieLocale,null,null);
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} 
		}
	}
	
	class clickSuperficie implements ActionListener {
		@SuppressWarnings("unused")
		public void actionPerformed(ActionEvent e) {
			try {
				partieLocale.changeTheme("cardline","superficie");
				System.out.println("type: "+partieLocale.getTypejeu().toString());
			} catch (IOException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}
			try {
				((Window) vue.getPane().getTopLevelAncestor()).dispose();
				ControleurJeu j=new ControleurJeu(new VueJeu(), partieLocale,null,null);
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} 
		}
	}
	
}
