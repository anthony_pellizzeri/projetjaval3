package controleur;

import java.awt.Color;
import java.awt.Desktop;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.filechooser.FileSystemView;
import model.Joueur;
import model.Partie;
import model.specification.NiveauRobot;
import vue.VueAjoutCarte;
import vue.VueChoixTheme;
import vue.VueJeu;
import vue.VueMenu;
import vue.VueMusique;

/**
 * 
 *gere le menu
 *
 */
public class ControleurMenu {
	private int JoueurSelectionne;
	private Partie partieLocal;
	private VueMenu vue;
	
	/**
	 * 
	 * @param partie
	 * @param vue
	 */
	@SuppressWarnings("deprecation")
	public ControleurMenu(Partie partie, VueMenu vue) {
		super();
		this.partieLocal = partie;

		this.vue = vue;
		vue.getBtnAnnuler().setVisible(false);
		vue.getMenuBarHaut().getMenuItemEnregistrer().addActionListener(new MenuEnregistrer());
		vue.getMenuBarHaut().getMenuItemAide().addActionListener(new MenuAide());
		vue.getMenuBarHaut().getMenuItemAjoutCarte().addActionListener(new MenuAjoutCarte());
		vue.getMenuBarHaut().getMenuItemMusique().addActionListener(new MenuMusique());
		vue.getMenuBarHaut().getMenuItemOuvrir().addActionListener(new MenuOuvrir());
		vue.getBtnSupprimer().addActionListener(new SupprimerJoueur());
		vue.getBtnAnnuler().addActionListener(new AnnulerSelection());
		vue.getBtnAjouter().addActionListener(new AjoutJoueur());
		vue.getCardLine().addActionListener(new cardLineJeu());
		vue.getTimeLine().addActionListener(new timeLineJeu());
		vue.getChoixNiveauAucun().addActionListener(new NiveauAucun());
		vue.getChoixNiveauFacile().addActionListener(new NiveauFacile());
		vue.getChoixNiveauMoyen().addActionListener(new NiveauMoyen());
		vue.getChoixNiveauDificile().addActionListener(new NiveauDificile());
		vue.getCellSelectionModel().addListSelectionListener(new ListSelectionListener() {
		      public void valueChanged(ListSelectionEvent e) {

			        int[] selectedRow = vue.getTable().getSelectedRows();
			        vue.getBtnAnnuler().setVisible(true);
			        vue.getBtnSupprimer().setVisible(true);
			        JoueurSelectionne=selectedRow[0];
			    	vue.getPaneNiveauRobot().setVisible(true);
		        	 vue.getChoixNiveauAucun().setSelected(false);
						vue.getChoixNiveauFacile().setSelected(false);
						vue.getChoixNiveauMoyen().setSelected(false);
						vue.getChoixNiveauDificile().setSelected(false);
						if(JoueurSelectionne>=partieLocal.getJoueurs().size()) {
							JoueurSelectionne=0;
						}
							switch (partieLocal.getJoueurs().get(JoueurSelectionne).getNiveauRobot()) {
							case aucun:
								vue.getChoixNiveauAucun().setSelected(true);
								break;
							case facile:
								vue.getChoixNiveauFacile().setSelected(true);
								break;
							case moyen:
								vue.getChoixNiveauMoyen().setSelected(true);
								break;
							case dificile:
								vue.getChoixNiveauDificile().setSelected(true);
								break;
							}
						
			      }

			    });
		
		int i=0;
		vue.getModel().getDataVector().clear();
		while(i<partieLocal.getJoueurs().size()) {
			Object[] joueurs = new Object[3]; 
			joueurs[0] = partieLocal.getJoueurs().get(i).getpseudo(); 
			joueurs[1] = partieLocal.getJoueurs().get(i).getAge().getDate()+"/"+(partieLocal.getJoueurs().get(i).getAge().getMonth()+1)+"/"+(partieLocal.getJoueurs().get(i).getAge().getYear()+1900);  
			joueurs[2] = partieLocal.getJoueurs().get(i).getPoint(); 
		    vue.getModel().addRow(joueurs);
		    i++;
		}
		
		
	}
	
	/**
	 * 
	 * ajout de joueur
	 *
	 */
	class AjoutJoueur implements ActionListener {
		@SuppressWarnings("deprecation")
		public void actionPerformed(ActionEvent e) {
			if(vue.getPseudo().getText().length()>0 && vue.getDatePicker().getJFormattedTextField().getText().length()>0 && partieLocal.getJoueurs().size()<8) {
				Date date = null;
				try {
					date = new SimpleDateFormat("dd/MM/yyyy").parse(vue.getDatePicker().getJFormattedTextField().getText());
				} catch (ParseException e1) {
	
				}			
				Joueur j=new Joueur(vue.getPseudo().getText(),date, null);
				partieLocal.addJoueur(j);
				
				partieLocal.trie();
				
				int i=0;
				vue.getModel().getDataVector().clear();
				while(i<partieLocal.getJoueurs().size()) {
					Object[] joueurs = new Object[3]; 
					joueurs[0] = partieLocal.getJoueurs().get(i).getpseudo(); 
					joueurs[1] = partieLocal.getJoueurs().get(i).getAge().getDate()+"/"+(partieLocal.getJoueurs().get(i).getAge().getMonth()+1)+"/"+(partieLocal.getJoueurs().get(i).getAge().getYear()+1900); 
					joueurs[2] = partieLocal.getJoueurs().get(i).getPoint(); 
					vue.getModel().addRow(joueurs);
				    i++;
				}
				
				vue.getPseudo().setText("");
	//			vue.getAge().setText("");
				vue.getDatePicker().getJFormattedTextField().setText("");
				vue.getAjoutJoueur().setForeground(Color.green);
				vue.getAjoutJoueur().setText("Joueur ajouté");
			}
			else if (partieLocal.getJoueurs().size()==8 && vue.getPseudo().getText().length()>0 && vue.getDatePicker().getJFormattedTextField().getText().length()>0) {
				vue.getAjoutJoueur().setForeground(Color.RED);
				vue.getAjoutJoueur().setText("Nombre max de joueur atteint");
			}
			else {
				vue.getAjoutJoueur().setForeground(Color.RED);
				vue.getAjoutJoueur().setText("erreur de saisie");
			}
		}
		
		
	}
	
	/**
	 * 
	 * supprime joueur selectionne
	 *
	 */
	class SupprimerJoueur implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			partieLocal.getJoueurs().remove(JoueurSelectionne);
			partieLocal.trie();
			int i=0;
			vue.getModel().getDataVector().clear();
			while(i<partieLocal.getJoueurs().size()) {
				Object[] joueurs = new Object[3]; 
				joueurs[0] = partieLocal.getJoueurs().get(i).getpseudo(); 
				joueurs[1] = partieLocal.getJoueurs().get(i).getAge(); 
				joueurs[2] = partieLocal.getJoueurs().get(i).getPoint(); 
				vue.getModel().addRow(joueurs);
			    i++;
			}
			vue.getBtnSupprimer().setVisible(false);
			vue.getPaneNiveauRobot().setVisible(false);
			vue.getBtnAnnuler().setVisible(false);
			vue.getAjoutJoueur().setText("");
		}
	}
	
	/**
	 * 
	 * annule le joueur et la saisi
	 *
	 */
	class AnnulerSelection implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			vue.getTable().getSelectionModel().addSelectionInterval(99,99);
			vue.getPseudo().setText("");
//			vue.getAge().setText("");
			vue.getDatePicker().getJFormattedTextField().setText("");

//			vue.getBtnAnnuler().setVisible(false);
			vue.getBtnSupprimer().setVisible(false);
			vue.getPaneNiveauRobot().setVisible(false);
			vue.getBtnAnnuler().setVisible(false);
			vue.getAjoutJoueur().setText("");
		}
	}
	
	/**
	 * 
	 * choix du jeu
	 *
	 */
	class cardLineJeu implements ActionListener {
		@SuppressWarnings("unused")
		public void actionPerformed(ActionEvent e) {
			if(partieLocal.getJoueurs().size()>1) {
				((Window) vue.getPaneJoueur().getTopLevelAncestor()).dispose();
				ControleurChoixTheme cardline=new ControleurChoixTheme(partieLocal, new VueChoixTheme()); 
			}
			else{
				vue.getAjoutJoueur().setForeground(Color.RED);
				vue.getAjoutJoueur().setText("au moins 2 joueurs");
			}
		}
	}
	
	/**
	 * 
	 * choix du jeu
	 *
	 */
	class timeLineJeu implements ActionListener {
		@SuppressWarnings("unused")
		public void actionPerformed(ActionEvent e) {
			if(partieLocal.getJoueurs().size()>1) {
				try {
					partieLocal.changeTheme("timeline", null);
				} catch (IOException e2) {
					// TODO Auto-generated catch block
					e2.printStackTrace();
				}
				try {
					((Window) vue.getPaneJoueur().getTopLevelAncestor()).dispose();
					ControleurJeu c=new ControleurJeu(new VueJeu(), partieLocal,null,null);
	//				VueJeu t=new VueJeu(partieLocal);
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} 
			}
			else{
				vue.getAjoutJoueur().setForeground(Color.RED);
				vue.getAjoutJoueur().setText("au moins 2 joueurs");
			}
			
		}
	}
	
	/**
	 * 
	 * difficulté du bot
	 *
	 */
	class NiveauAucun implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			vue.getChoixNiveauAucun().setSelected(true);
			vue.getChoixNiveauFacile().setSelected(false);
			vue.getChoixNiveauMoyen().setSelected(false);
			vue.getChoixNiveauDificile().setSelected(false);
			partieLocal.getJoueurs().get(JoueurSelectionne).setNiveauRobot(NiveauRobot.aucun);
		}
	}
	
	
	/**
	 * 
	 * difficulté du bot
	 *
	 */
	class NiveauFacile implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			vue.getChoixNiveauAucun().setSelected(false);
			vue.getChoixNiveauFacile().setSelected(true);
			vue.getChoixNiveauMoyen().setSelected(false);
			vue.getChoixNiveauDificile().setSelected(false);
			partieLocal.getJoueurs().get(JoueurSelectionne).setNiveauRobot(NiveauRobot.facile);
			
		}
	}
	
	/**
	 * 
	 * difficulté du bot
	 *
	 */
	class NiveauMoyen implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			vue.getChoixNiveauAucun().setSelected(false);
			vue.getChoixNiveauFacile().setSelected(false);
			vue.getChoixNiveauMoyen().setSelected(true);
			vue.getChoixNiveauDificile().setSelected(false);
			partieLocal.getJoueurs().get(JoueurSelectionne).setNiveauRobot(NiveauRobot.moyen);
			
		}
	}
	
	
	/**
	 * 
	 * difficulté du bot
	 *
	 */
	class NiveauDificile implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			vue.getChoixNiveauAucun().setSelected(false);
			vue.getChoixNiveauFacile().setSelected(false);
			vue.getChoixNiveauMoyen().setSelected(false);
			vue.getChoixNiveauDificile().setSelected(true);
			partieLocal.getJoueurs().get(JoueurSelectionne).setNiveauRobot(NiveauRobot.dificile);
		}
	}
	
	/**
	 * 
	 * enregistrer une partie
	 *
	 */
	private class MenuEnregistrer implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			JFileChooser fileChooser = new JFileChooser();
			fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
			FileNameExtensionFilter filtre = new FileNameExtensionFilter("Jeu", "jeu");
			fileChooser.addChoosableFileFilter(filtre);
			fileChooser.setFileFilter(filtre);
			fileChooser.setCurrentDirectory(FileSystemView.getFileSystemView().getHomeDirectory());
			int result = fileChooser.showSaveDialog(fileChooser);
			if (result == JFileChooser.APPROVE_OPTION) {
				try {
					FileOutputStream f = new FileOutputStream(fileChooser.getSelectedFile() + ".jeu");
					ObjectOutputStream s = new ObjectOutputStream(f);
					s.writeObject(partieLocal);
					s.flush();
					s.close();
				} catch (Exception exception) {
					JOptionPane.showMessageDialog(null, exception.getMessage(), "Erreur", JOptionPane.ERROR_MESSAGE);
				}
			}
		}
	}

	/**
	 * 
	 * ouvrir une partie enregistrer
	 */
	private class MenuOuvrir implements ActionListener {

		@SuppressWarnings("deprecation")
		@Override
		public void actionPerformed(ActionEvent e) {
			JFileChooser fileChooser = new JFileChooser();
			fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
			FileNameExtensionFilter filtre = new FileNameExtensionFilter("Jeu", "jeu");
			fileChooser.addChoosableFileFilter(filtre);
			fileChooser.setFileFilter(filtre);
			fileChooser.setCurrentDirectory(FileSystemView.getFileSystemView().getHomeDirectory());
			int result = fileChooser.showOpenDialog(fileChooser);
			if (result == JFileChooser.APPROVE_OPTION) {
				try {
					FileInputStream f = new FileInputStream(fileChooser.getSelectedFile());
					ObjectInputStream s = new ObjectInputStream(f);
					partieLocal = new Partie((Partie) s.readObject());
					int i=0;
					vue.getModel().getDataVector().clear();
					while(i<partieLocal.getJoueurs().size()) {
						Object[] joueurs = new Object[3]; 
						joueurs[0] = partieLocal.getJoueurs().get(i).getpseudo(); 
						joueurs[1] = partieLocal.getJoueurs().get(i).getAge().getDate()+"/"+(partieLocal.getJoueurs().get(i).getAge().getMonth()+1)+"/"+(partieLocal.getJoueurs().get(i).getAge().getYear()+1900); 
						joueurs[2] = partieLocal.getJoueurs().get(i).getPoint(); 
					    vue.getModel().addRow(joueurs);
					    i++;
					}
					f.close();
					s.close();
				} catch (Exception exception) {
					JOptionPane.showMessageDialog(null, exception.getMessage(), "Erreur", JOptionPane.ERROR_MESSAGE);
				}
			}
		}
	}
	
	/**
	 * 
	 * ouvrir le PDF de l'aide du jeu
	 *
	 */
	private class MenuAide implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			File f=new File("data/regle.pdf");
			try {
				Desktop.getDesktop().open(f);
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
		}
	}
	
	/**
	 * 
	 * vouloir ajouter une carte
	 *
	 */
	private class MenuAjoutCarte implements ActionListener {
		@SuppressWarnings("unused")
		@Override
		public void actionPerformed(ActionEvent e) {
			((Window) vue.getPaneJoueur().getTopLevelAncestor()).dispose();
			try {
				ControleurAjoutCarte c=new ControleurAjoutCarte(new VueAjoutCarte(), partieLocal);
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	}
	
	/**
	 * 
	 * vouloir changer la musique
	 *
	 */
	private class MenuMusique implements ActionListener {
		@SuppressWarnings("unused")
		@Override
		public void actionPerformed(ActionEvent e) {
			((Window) vue.getPaneJoueur().getTopLevelAncestor()).dispose();
			ControleurMusique c=new ControleurMusique(partieLocal,new VueMusique());
		}
	}
}
