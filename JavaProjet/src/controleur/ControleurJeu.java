package controleur;


import java.awt.Dimension;
import java.awt.Image;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.Timer;
import model.Carte;
import model.Joueur;
import model.Partie;
import model.specification.NiveauRobot;
import model.specification.typeJeu;
import vue.VueChangementTheme;
import vue.VueGagnants;
import vue.VueJeu;

/**
 * 
 * gerer le jeu
 *
 */
public class ControleurJeu {
	private int choixMain = -1;
	private int choixTapis = -1;
	private ArrayList<Joueur> listGagnants = new ArrayList<Joueur>();
	private int posJoueur = 0;
	private Partie partieLocal;
	private boolean sortir = false;
	private VueJeu vue;
	private ArrayList<JButton> gaucheDroite = new ArrayList<JButton>();
	private ArrayList<JButton> CarteTapis = new ArrayList<JButton>();
	private ArrayList<JButton> carteMain = new ArrayList<JButton>();
	private int temps;
	private int tempsMAxJoueur = 40;
	private Timer jolieTimer;
	private boolean pasTimer=false;

	/**
	 * lancer le jeu ainsi que le timer et les bots si il y en a au debut
	 * @param vue
	 * @param partie
	 * @param pos
	 * @param listGagnants
	 */
	@SuppressWarnings("unused")
	public ControleurJeu(VueJeu vue, Partie partie,Integer pos, ArrayList<Joueur> listGagnants) {
		super();
		this.partieLocal = partie;
		this.vue = vue;
		if(partieLocal.getTapis().size()==0) {
			partieLocal.distribCartes();
			partieLocal.trie();
			partieLocal.premiereCarte();
		}else {
			this.posJoueur=pos;
			this.listGagnants=listGagnants;
		}
		
		
		if(partieLocal.getTypejeu()==typeJeu.timeline) {
			vue.getChangeTheme().setEnabled(false);
		}else {
			vue.getChangeTheme().setEnabled(true);
		}
		
		
		if(partieLocal.getTypejeu()==typeJeu.cardline) {
			vue.getInfoJeu().setText("Jeu: "+partieLocal.getTypejeu().toString()+" | thême: "+partieLocal.getThemeJeu().toString());
		}else {
			vue.getInfoJeu().setText("Jeu: "+partieLocal.getTypejeu().toString());
		}
		// si c'est que des robot pas lancer timer
		for(Joueur j: partieLocal.getJoueurs()) {
			if(j.getNiveauRobot()!= NiveauRobot.aucun) {
				pasTimer=true;
			}
		}

		if (partieLocal.getJoueurs().size() > 1) {
			robot();
		}

		if (sortir != true) {

			afficheCarteTapis();
			afficheCarteMain();
			bonus();
			vue.getPseudo().setText("pseudo: " + partieLocal.getJoueurs().get(posJoueur).getpseudo());
		}

		if (sortir == true) {
			if(pasTimer==false) {
				jolieTimer.stop();
			}
			((JFrame) vue.getPaneJoueur().getTopLevelAncestor()).dispose();
			ControleurGagnant g = new ControleurGagnant(new VueGagnants(), partieLocal);
		}

		vue.getBonusAppel().addActionListener(new bonusAppel());
		vue.getChangeTheme().addActionListener(new changerTheme());

		temps = tempsMAxJoueur;
		int delay = 1000; // milliseconds
		if(pasTimer==false) {
			jolieTimer = new Timer(delay, new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					// TODO Auto-generated method stub
					temps--;
					vue.getTemps().setText("Temps restant: " + Integer.toString(temps));
					if (temps == 0) {	
						posJoueur++;
						posJoueur = posJoueur % (partieLocal.getJoueurs().size());
						afficheCarteTapis();
						afficheCarteMain();
						temps = tempsMAxJoueur;
					}
					if (sortir != true) {
						bonus();
					}

				}
			});
			jolieTimer.start();
		}
		
	}

	/**
	 * afficher carte du tapis
	 */
	public void afficheCarteTapis() {
		vue.getPaneTapis().removeAll();
		JButton gauche = new JButton();
		gauche.setActionCommand("0");
		gauche.addActionListener(new clickGaucheDroite());
		gaucheDroite.clear();
		gaucheDroite.add(gauche);
		vue.getPaneTapis().add(gauche);
		gauche.setVisible(false);
		gauche.setPreferredSize(new Dimension(40, 40));
		CarteTapis.clear();
		int n;
		for (int i = 0; i < partieLocal.getTapis().size(); i++) {
			ImageIcon imageIcon = new ImageIcon("data/" + partieLocal.getTypejeu().toString() + "/cards/"
					+ partieLocal.getTapis().get(i).getImgFaceOuverte()); // load the image to a imageIcon
			Image image = imageIcon.getImage();
			Image newimg = image.getScaledInstance(300, 300, java.awt.Image.SCALE_SMOOTH);
			imageIcon = new ImageIcon(newimg);
			JButton button = new JButton();

			button.setIcon(imageIcon);
			button.setActionCommand(Integer.toString(i));
			button.addActionListener(new clickTapis());
			button.setEnabled(false);
			button.setDisabledIcon(imageIcon);
			CarteTapis.add(button);
			vue.getPaneTapis().add(button);
			JButton droite = new JButton();
			n = i + 1;

			droite.setActionCommand(Integer.toString(n));
			droite.addActionListener(new clickGaucheDroite());
			droite.setPreferredSize(new Dimension(40, 40));
			gaucheDroite.add(droite);
			vue.getPaneTapis().add(droite);
			droite.setVisible(false);
		}
	}

	/**
	 * afficher carte de la main du joueur en question 
	 */
	public void afficheCarteMain() {
		vue.getPaneJoueur().removeAll();
		for (int i = 0; i < partieLocal.getJoueurs().get(posJoueur).getMain().size(); i++) {
			ImageIcon imageIcon = new ImageIcon("data/" + partieLocal.getTypejeu().toString() + "/cards/"
					+ partieLocal.getJoueurs().get(posJoueur).getMain().get(i).getImgFaceCache()); // load the image
																										// to a
																										// imageIcon
			Image image = imageIcon.getImage();
			Image newimg = image.getScaledInstance(280, 280, java.awt.Image.SCALE_SMOOTH);
			imageIcon = new ImageIcon(newimg);
			JButton button = new JButton();
			button.setIcon(imageIcon);
			button.setActionCommand(Integer.toString(i));
			button.addActionListener(new clickMain());
			button.addMouseListener((MouseListener) new MouseListener() {
				@Override
				public void mouseClicked(MouseEvent e) {
				}

				public void mousePressed(MouseEvent e) {
				}

				@Override
				public void mouseReleased(MouseEvent e) {
				}

				@Override
				public void mouseEntered(MouseEvent e) {
					button.setBounds(button.getX(), button.getY() - 10, button.getWidth(), button.getHeight());
				}

				@Override
				public void mouseExited(MouseEvent e) {
					button.setBounds(button.getX(), button.getY() + 10, button.getWidth(), button.getHeight());
				}
			});
			vue.getPaneJoueur().add(button);
			carteMain.add(button);
		}
		vue.getPseudo().setText("pseudo: " + partieLocal.getJoueurs().get(posJoueur).getpseudo());
		tableauAutreJoueur();
	}

	/**
	 * bonus
	 */
	public void bonus() {
		if (!partieLocal.getJoueurs().get(posJoueur).isBonusAppel()) {
			vue.getBonusAppel().setEnabled(false);
		} else {
			vue.getBonusAppel().setEnabled(true);
		}
	}
	
	/**
	 * 
	 * davoir la carte click de la main
	 *
	 */
	class clickMain implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			choixMain = Integer.parseInt(e.getActionCommand());
			for (int i = 0; i < CarteTapis.size(); i++) {
				CarteTapis.get(i).setEnabled(true);
			}
		}
	}

	/**
	 * savoir la carte click du tapis
	 *
	 */
	class clickTapis implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			choixTapis = Integer.parseInt(e.getActionCommand());

			for (int i = 0; i < gaucheDroite.size(); i++) {
				gaucheDroite.get(i).setVisible(false);
			}
			gaucheDroite.get(choixTapis).setVisible(true);
			gaucheDroite.get(choixTapis + 1).setVisible(true);

		}
	}
	
	/**
	 * 
	 * poser la carte sur la tapis et faire les tests de bon placement
	 * ajouter list gagnants 
	 * finir partie
	 *
	 */
	class clickGaucheDroite implements ActionListener {
		@SuppressWarnings("unused")
		public void actionPerformed(ActionEvent e) {
			int posCarteTapis = Integer.parseInt(e.getActionCommand());
			for (int i = 0; i < gaucheDroite.size(); i++) {
				gaucheDroite.get(i).setVisible(false);
			}
			for (int i = 0; i < CarteTapis.size(); i++) {
				CarteTapis.get(i).setEnabled(false);
			}
			
			Carte carteUtilise = new Carte(partieLocal.getJoueurs().get(posJoueur).getMain().get(choixMain));
			partieLocal.ajoutCarte(posJoueur, posCarteTapis,
					partieLocal.getJoueurs().get(posJoueur).getMain().get(choixMain));

//			partieLocal.testPlacement(posJoueur, carteUtilise, Integer.parseInt(e.getActionCommand()));
			
			partieLocal.testPlacement(posJoueur, partieLocal.getCarteJeu().get(posCarteTapis), Integer.parseInt(e.getActionCommand()));

			if (partieLocal.mainVide(posJoueur, listGagnants)) {
				posJoueur--;
			}

			if (posJoueur == (partieLocal.getJoueurs().size() - 1) && listGagnants.size() > 0) {
				// que 1 gagnant
				// plus de carte a jouer
				partieLocal.partieFinale(listGagnants);
				if (partieLocal.getJoueurs().get(0).getMain().size() == 0 && partieLocal.getJoueurs().size() != 1) {
					sortir = true;
					partieLocal.egalitePoint();
				}

				if (partieLocal.getJoueurs().size() == 1) {
					if(pasTimer==false) {
						jolieTimer.stop();
					}
					((JFrame) vue.getPaneJoueur().getTopLevelAncestor()).dispose();
					ControleurGagnant g = new ControleurGagnant(new VueGagnants(), partieLocal);
				} else {
					posJoueur = -1;
				}

			}
			posJoueur++;
			posJoueur = posJoueur % (partieLocal.getJoueurs().size());

			if (partieLocal.getJoueurs().size() >= 1) {
				robot();
			}

			if (sortir != true) {
				afficheCarteTapis();
				afficheCarteMain();
				bonus();
			}

			if (sortir == true) {
				if(pasTimer==false) {
					jolieTimer.stop();
				}
				((JFrame) vue.getPaneJoueur().getTopLevelAncestor()).dispose();
				ControleurGagnant g = new ControleurGagnant(new VueGagnants(), partieLocal);
			} else {
				afficheCarteTapis();
				vue.getPaneTapis().revalidate();
				vue.getPaneTapis().repaint();
				afficheCarteMain();
				bonus();
				vue.getPaneJoueur().revalidate();
				vue.getPaneJoueur().repaint();
			}
			temps = tempsMAxJoueur;

		}
	}

	/**
	 * lancer le ou les robots
	 */
	@SuppressWarnings("unused")
	public void robot() {
		boolean fini = false;
		int i = 0;
		while (!partieLocal.getJoueurs().get(posJoueur).getNiveauRobot().equals(NiveauRobot.aucun) && fini == false) {
			if (partieLocal.getJoueurs().get(posJoueur).getMain().size() > 0) {
				model.Robot r = new model.Robot(partieLocal, posJoueur);
			}

			if (partieLocal.mainVide(posJoueur, listGagnants)) {
				posJoueur--;
			}
			if (posJoueur == (partieLocal.getJoueurs().size() - 1) && listGagnants.size() > 0) {
				// que 1 gagnant
				partieLocal.partieFinale(listGagnants);
				if (partieLocal.getJoueurs().get(0).getMain().size() == 0 && partieLocal.getJoueurs().size() != 1) {
					sortir = true;
					partieLocal.egalitePoint();
					break;
				}

				if (partieLocal.getJoueurs().size() == 1) {
					sortir = true;
					fini = true;
				} else {
					posJoueur = -1;
				}

			}

			posJoueur++;
			posJoueur = posJoueur % (partieLocal.getJoueurs().size());
			i++;
		}
		temps = tempsMAxJoueur;

	}

	/**
	 * 
	 *click bonus appel
	 */
	class bonusAppel implements ActionListener {
		@SuppressWarnings("static-access")
		public void actionPerformed(ActionEvent e) {
			try {
				partieLocal.getJoueurs().get(posJoueur).setBonusAppel(false);
				String[] amis = { "Julien", "Quentin", "Anthony" };
				JOptionPane jop = new JOptionPane(), jop2 = new JOptionPane();
				int rang = jop.showOptionDialog(null, "Veuillez choisir votre ami contacter!", "Bonus appel ami",
						JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null, amis, amis[2]);
				int choixMain = (int) (Math.random() * partieLocal.getJoueurs().get(posJoueur).getMain().size());
				int choixAmi = partieLocal.bonusAppel(posJoueur, amis[rang], choixMain);
				jop2.showMessageDialog(null,
						"votre ami: " + amis[rang] + " vous conseil de placer la carte n " + choixMain
								+ " sur le bouton numero " + choixAmi,
						"vous conseil ", JOptionPane.INFORMATION_MESSAGE);

			} catch (Exception e1) {
				// TODO: handle exception
			}

		}
	}
	
	/**
	 * gerer le tableau des joueurs
	 */
	public void tableauAutreJoueur() {
		int i=0;
		String texte="joueurs en liste: "+'\n' + '\r';
		while(i<partieLocal.getJoueurs().size()) {
			texte+=partieLocal.getJoueurs().get(i).getPseudo()+", reste "+partieLocal.getJoueurs().get(i).getMain().size()+" carte(s)"+'\n' + '\r';
		    i++;
		}
		texte+=" "+'\n' + '\r';
		texte+="Les gagnants:"+'\n' + '\r';
		i=0;
		while(i<listGagnants.size()) {
			texte+=listGagnants.get(i).getPseudo();
		    i++;
		}
		
		vue.getTextAutreJoueur().setText(texte);
	}
	
	/**
	 * changer de theme en cours de partie
	 *
	 */
	class changerTheme implements ActionListener {
		@SuppressWarnings("unused")
		public void actionPerformed(ActionEvent e) {
			if(pasTimer==false) {
				jolieTimer.stop();
			}
			((Window) vue.getPaneTapis().getTopLevelAncestor()).dispose();
			ControleurChangementTheme g = new ControleurChangementTheme(partieLocal,new VueChangementTheme(),posJoueur,listGagnants);

		}
	}
	
	
}
