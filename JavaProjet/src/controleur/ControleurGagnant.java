package controleur;

import java.awt.Dimension;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;

import model.Joueur;
import model.Partie;
import vue.VueGagnants;
import vue.VueJeu;
import vue.VueMenu;

/**
 * 
 * gerer les gagnant
 *
 */
public class ControleurGagnant {
	private VueGagnants vue;
	private Partie partielocale;
	
	/**
	 * 
	 * @param vue
	 * @param partie
	 */
	public ControleurGagnant(VueGagnants vue, Partie partie) {
		super();
		this.vue = vue;
		this.partielocale = partie;
		ArrayList<Joueur> gagnants= partie.getJoueurs();
		vue.getMenu().addActionListener(new menu());
		vue.getRecommencer().addActionListener(new recomencer());
		
		if(gagnants.size()==1) {
			vue.getLblgagnant().setText("gagnant: "+gagnants.get(0).getpseudo());
		}else {
			String s="";
			for(int i=0;i<gagnants.size();i++) {
				s+=gagnants.get(i).getpseudo()+" ";
			}
			vue.getLblgagnant().setText("égalité entre: "+s);
		}
		
		partielocale.recommencerJeu();
		int i=0;
		vue.getModel().getDataVector().clear();
		while(i<partielocale.getJoueurs().size()) {
			Object[] joueurs = new Object[2]; 
			joueurs[0] = partielocale.getJoueurs().get(i).getpseudo(); 
			joueurs[1] = partielocale.getJoueurs().get(i).getPoint(); 
		    vue.getModel().addRow(joueurs);
		    i++;
		}
		vue.getTable().setPreferredSize(new Dimension(40, 60));
		
	}
	

	/**
	 * 
	 * recommencer la partie
	 *
	 */
	class recomencer implements ActionListener {
		@SuppressWarnings("unused")
		public void actionPerformed(ActionEvent e) {
			try {
				((Window) vue.getPaneClassement().getTopLevelAncestor()).dispose();
				ControleurJeu j=new ControleurJeu(new VueJeu(), partielocale,null,null);
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} 
			
		}
	}
	
	/**
	 * 
	 * retour au menu
	 *
	 */
	class menu implements ActionListener {
		@SuppressWarnings("unused")
		public void actionPerformed(ActionEvent e) {
			((Window) vue.getPaneClassement().getTopLevelAncestor()).dispose();
			ControleurMenu c=new ControleurMenu(partielocale, new VueMenu());
		}
	}
	

}
