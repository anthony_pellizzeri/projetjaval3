package controleur;

import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.Timer;

import model.Partie;
import vue.VueMenu;
import vue.VueMusique;

/**
 * 
 * gerer la musique
 *
 */
public class ControleurMusique {
	private Partie partieLocale;
	private VueMusique vue;
	private int temps;
	private int tempsMax = 660; //11 minutes la musique
	private Timer jolieTimerMusique;
	private Clip clip;
	private boolean app=false;
	
	
	/**
	 * 
	 * @param partie
	 * @param vue
	 */
	public ControleurMusique(Partie partie, VueMusique vue) {
		super();
		this.partieLocale = partie;
		this.vue = vue;
		vue.getMusiqueOn().addActionListener(new clickMusiqueOn());
		vue.getRetourMenu().addActionListener(new clickRetourMenu());
	}
	
	/**
	 * 
	 * mettre ou enlever de la musique
	 *
	 */
	private class clickMusiqueOn implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			if(app) {
				clip.stop();
				app=false;
				
			}else {
				String soundName = "data/sound/musique.wav";
				AudioInputStream audioInputStream = null;
				try {
				audioInputStream = AudioSystem.getAudioInputStream(new File(soundName).getAbsoluteFile());
				clip = AudioSystem.getClip();
				clip.open(audioInputStream);
				} catch (UnsupportedAudioFileException | IOException e1) {

				} catch (LineUnavailableException e1) {
		
				}
					clip.start();
					app=true;
					
					temps = tempsMax;
					int delay = 1000; // milliseconds
						jolieTimerMusique = new Timer(delay, new ActionListener() {

							@Override
							public void actionPerformed(ActionEvent e) {
								// TODO Auto-generated method stub
								temps--;
								if(temps==0) { //boucler sur la musique
									clip.stop();
									clip.start();
									temps=tempsMax;
								}
							}
						});
						jolieTimerMusique.start();	
			}
			
			
			}
	}
	
	/**
	 * 
	 * retourner au menu
	 *
	 */
	private class clickRetourMenu implements ActionListener {
		@SuppressWarnings("unused")
		public void actionPerformed(ActionEvent e) {
			((Window) vue.getPane().getTopLevelAncestor()).dispose();
			ControleurMenu j=new ControleurMenu(partieLocale,new VueMenu()); 
		}
	}
	
}
