package controleur;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.StringTokenizer;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import model.Partie;
import vue.VueAjoutCarte;
import vue.VueMenu;

public class ControleurAjoutCarte {
	private Partie partieLocale;
	private VueAjoutCarte vue;
	private  ImageIcon imageCardLineOuverte = new ImageIcon();
	private  ImageIcon imageTimeLineFerme = new ImageIcon();
	private  ImageIcon imageCardLineFerme = new ImageIcon();
	private  ImageIcon imageTimeLineOuverte = new ImageIcon();
	
	/**
	 * 
	 * @param vue
	 * @param partie
	 * @throws IOException
	 */
	public ControleurAjoutCarte(VueAjoutCarte vue,Partie partie) throws IOException {
		super();
		this.partieLocale = partie;
		this.vue = vue;
		vue.getChercherCarteCardLineCache().addActionListener(new ajoutCarteCardLineCache());
		vue.getChercherCarteCardLineOuverte().addActionListener(new ajoutCarteCardLineOuvert());
		
		vue.getChercherCarteTimelineCache().addActionListener(new ajoutCarteTimeLineCache());
		vue.getChercherCarteTimelineOuvert().addActionListener(new ajoutCarteTimeLineOuvert());
		
		
		vue.getAjouterCardline().addActionListener(new ajoutCardLine());
		vue.getAjouterTimeLine().addActionListener(new ajoutTimeLine());
		
		vue.getRetourMenu1().addActionListener(new retourMenu());
		vue.getRetourMenu2().addActionListener(new retourMenu());
	}
	
	/**
	 * 
	 * ajout immage
	 *
	 */
	class ajoutCarteCardLineCache implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			 JFileChooser fc = new JFileChooser();
			 FileFilter imagesFilter = new FileNameExtensionFilter("jpg", "jpeg");
			 fc.addChoosableFileFilter(imagesFilter);
		        int result = fc.showOpenDialog(null);
		        if (result == JFileChooser.APPROVE_OPTION) {
		            File file = fc.getSelectedFile();
		            String sname = file.getAbsolutePath(); 
		            imageCardLineFerme.setImage(new ImageIcon(sname).getImage().getScaledInstance(90, 100, Image.SCALE_DEFAULT));
		            vue.getImageCardlineCache().setIcon(imageCardLineFerme);
		        }
		}
	}
	
	/**
	 * 
	 *ajout image
	 *
	 */
	class ajoutCarteCardLineOuvert implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			 JFileChooser fc = new JFileChooser();
			 FileFilter imagesFilter = new FileNameExtensionFilter("jpg", "jpeg");
			 fc.addChoosableFileFilter(imagesFilter);
		        int result = fc.showOpenDialog(null);
		        if (result == JFileChooser.APPROVE_OPTION) {
		            File file = fc.getSelectedFile();
		            String sname = file.getAbsolutePath(); 
		            imageCardLineOuverte.setImage(new ImageIcon(sname).getImage().getScaledInstance(90, 100, Image.SCALE_DEFAULT));
		            vue.getImageCardlineOuvert().setIcon(imageCardLineOuverte);

		        }
		}
	}
	
	/**
	 * 
	 *ajout image
	 *
	 */
	class ajoutCarteTimeLineCache implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			 JFileChooser fc = new JFileChooser();
			 FileFilter imagesFilter = new FileNameExtensionFilter("jpg", "jpeg");
			 fc.addChoosableFileFilter(imagesFilter);
		        int result = fc.showOpenDialog(null);
		        if (result == JFileChooser.APPROVE_OPTION) {
		            File file = fc.getSelectedFile();
		            String sname = file.getAbsolutePath(); 
		            imageTimeLineFerme.setImage(new ImageIcon(sname).getImage().getScaledInstance(90, 100, Image.SCALE_DEFAULT));
		            vue.getImageTimelineCache().setIcon(imageTimeLineFerme);

		        }
		}
	}
	
	/**
	 * 
	 *ajout image
	 *
	 */
	class ajoutCarteTimeLineOuvert implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			 JFileChooser fc = new JFileChooser();
			 FileFilter imagesFilter = new FileNameExtensionFilter("jpg", "jpeg");
			 fc.addChoosableFileFilter(imagesFilter);
		        int result = fc.showOpenDialog(null);
		        if (result == JFileChooser.APPROVE_OPTION) {
		            File file = fc.getSelectedFile();
		            String sname = file.getAbsolutePath(); 
		            imageTimeLineOuverte.setImage(new ImageIcon(sname).getImage().getScaledInstance(90, 100, Image.SCALE_DEFAULT));
		            vue.getImageTimelineOuvert().setIcon(imageTimeLineOuverte);

		        }
		}
	}

/**
 * 
 * ajout carte dans le jeu (CSV et data(img))
 *
 */
	class ajoutCardLine implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			String auxPIB=vue.getCardline().getPib().getText();
			String auxPollution=vue.getCardline().getPollution().getText();
			String auxPopulation=vue.getCardline().getPopulation().getText();
			String auxSuperficie=vue.getCardline().getSuperficie().getText();
			String auxVille=vue.getCardline().getVille().getText();
			
			if(estUnEntier(auxPIB) && estUnEntier(auxPollution) && estUnEntier(auxPopulation) && estUnEntier(auxSuperficie)  && auxVille.length()>0 && vue.getImageCardlineCache().getIcon()!=null && vue.getImageCardlineOuvert().getIcon()!=null) {
				Integer photo=0;
				try {
					photo=testEntierCarte("cardline")+1;
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				photo=photo+1;
				imageToFile("cardline", photo.toString());
				String content=vue.getCardline().getVille().getText()+";"+vue.getCardline().getSuperficie().getText()+";"+vue.getCardline().getPopulation().getText()+";"+vue.getCardline().getPib().getText()+";"+vue.getCardline().getPollution().getText()+";"+photo.toString()+"\n";
				ecrireCSV("cardline",content);
				
				vue.getCardline().getLblValidationCardLine().setForeground(Color.green);
				vue.getCardline().getLblValidationCardLine().setText("Ajouté");
			}else {
				vue.getCardline().getLblValidationCardLine().setForeground(Color.red);
				vue.getCardline().getLblValidationCardLine().setText("erreur de saisie");
			}
			
		}
	}
	
	/**
	 * 
	 * ajout carte dans le jeu (CSV et data(img))
	 *
	 */
	class ajoutTimeLine implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			if(estUnEntier(vue.getTimeline().getDate().getText()) && vue.getTimeline().getInvention().getText().length()>0 &&  vue.getImageTimelineCache().getIcon()!=null && vue.getImageTimelineOuvert().getIcon()!=null) {
				Integer photo=0;
				try {
					photo=testEntierCarte("timeline")+1;
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				photo=photo+1;
				imageToFile("timeline", photo.toString());
				String content=vue.getTimeline().getInvention().getText()+";"+vue.getTimeline().getDate().getText()+";"+photo.toString()+"\n";
				ecrireCSV("timeline",content);
				vue.getTimeline().getLblValidationTimeLine().setForeground(Color.green);
				vue.getTimeline().getLblValidationTimeLine().setText("Ajouté");
			}
			else 
			{
				vue.getTimeline().getLblValidationTimeLine().setForeground(Color.red);
				vue.getTimeline().getLblValidationTimeLine().setText("erreur de saisie");
			}
		}	
	}
	
/**
 * 
 * @param typeCarte
 * @param name
 * importer en lcoal l'image dans les datas
 */
 private void imageToFile(String typeCarte,String name) {
    try {
    	
    if(typeCarte=="timeline") {
    	BufferedImage outputOuvert=toBufferedImage(imageTimeLineOuverte.getImage());
    	File ficNormal= new File("data/"+typeCarte+"/cards/"+name+ ".jpeg");
    	ImageIO.write(outputOuvert, "jpg", ficNormal);
    	
    	BufferedImage outputCache=toBufferedImage(imageTimeLineFerme.getImage());
    	File ficPerso= new File("data/"+typeCarte+"/cards/"+name+ "_date.jpeg");
    	ImageIO.write(outputCache, "jpg", ficPerso);
    }else {
    	BufferedImage outputOuvert=toBufferedImage(imageCardLineOuverte.getImage());
    	File ficNormal= new File("data/"+typeCarte+"/cards/"+name+ ".jpeg");
    	ImageIO.write(outputOuvert, "jpg", ficNormal);
    	
    	BufferedImage outputCache=toBufferedImage(imageCardLineFerme.getImage());
    	File ficPerso= new File("data/"+typeCarte+"/cards/"+name+ "_reponse.jpeg");
    	 ImageIO.write(outputCache, "jpg", ficPerso);
    }
    } catch (IOException e) {
        e.printStackTrace();
    }

}
 
/**
 * 
 * @param image
 * @return image stocké
 */
private BufferedImage toBufferedImage(Image image) {
	//  test si l'image n'est pas déja une instance de BufferedImage 
	if( image instanceof BufferedImage ) {
	        return( (BufferedImage)image );
	} else {
	        image = new ImageIcon(image).getImage();
	
	        //  crée la nouvelle image 
	        BufferedImage bufferedImage = new BufferedImage(
	                                              image.getWidth(null),
	                                              image.getHeight(null),
	                                              BufferedImage.TYPE_INT_RGB );
	        Graphics g = bufferedImage.createGraphics();
	        g.drawImage(image,0,0,null);
	        g.dispose();
	
	        return( bufferedImage );
	    } 
	}

	/**
	 * ecrire dans le CSV
	 * @param jeu
	 * @param Content
	 */
	private void ecrireCSV(String jeu,String Content) {
		String chemin = "data/"+jeu+"/"+jeu+".csv";
		File file = new File(chemin);
		try 
		{
		    RandomAccessFile random = new RandomAccessFile(file, "rw");
		    long length = random.length();
		    random.setLength(length + 1);
		    random.seek(random.length());
		    random.writeBytes(Content);
		    random.close();
		} 
		catch (Exception exception) {
		    //exception handling
		}
	}
	
	/**
	 * test si le string est un entier 
	 * @param chaine
	 * @return
	 */
	public boolean estUnEntier(String chaine) {
		try {
			Integer.parseInt(chaine);
		} catch (NumberFormatException e){
			return false;
		}
 
		return true;
	}


	/**
	 * test si c'est un entier pour ne pas avoir de doublons de nom
	 * @param jeu
	 * @return
	 * @throws IOException
	 */
	@SuppressWarnings("unused")
	private int testEntierCarte(String jeu) throws IOException {
				
		String chemin = "data/"+jeu+"/"+jeu+".csv";
		int i = 0;
		File file = new File(chemin);
		FileReader fr = new FileReader(file);
		BufferedReader br = new BufferedReader(fr);
		StringTokenizer st = null;
		br.readLine();
		String img;
		int nbr=0;
		for (String line = br.readLine(); line != null; line = br.readLine()) {
			st = new StringTokenizer(line, ";");
			String illustration = st.nextToken();
			if (jeu =="cardline") {
				st.nextToken();
				st.nextToken();
				st.nextToken();
				st.nextToken();
				img = st.nextToken();
				Double themeCardLine = 0.0;
				if(estUnEntier(img)) {
					nbr++;
				}
			} else {
				String themeTimeLine = st.nextToken();
				img = st.nextToken();
				if(estUnEntier(img)) {
					nbr++;
				}
			}
			i++;
		}
		br.close();
		
		
		return nbr;
		
	}
	
	/**
	 * retourner au menu
	 *
	 */
	class retourMenu implements ActionListener {
		@SuppressWarnings("unused")
		public void actionPerformed(ActionEvent e) {
			((Window) vue.getCardline().getTopLevelAncestor()).dispose();
			ControleurMenu j=new ControleurMenu(partieLocale,new VueMenu());
		}
	}
	
}

