package controleur;

import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;

import model.Joueur;
import model.Partie;
import vue.VueChangementTheme;
import vue.VueJeu;
/**
 * 
 * gerer le changement de theme
 *
 */
public class ControleurChangementTheme {
	private Partie partieLocale;
	private VueChangementTheme vue;
	private int posjoueur;
	private ArrayList<Joueur> listGagnants;
	
	/**
	 * 
	 * @param partie
	 * @param vue
	 * @param posJoueur
	 * @param listGagnants
	 */
	public ControleurChangementTheme(Partie partie, VueChangementTheme vue,int posJoueur, ArrayList<Joueur> listGagnants) {
		super();
		this.partieLocale = partie;
		this.vue=vue;
		this.posjoueur=posJoueur;
		this.listGagnants=listGagnants;
		
		vue.getPib().addActionListener(new clickPIB());
		vue.getPollution().addActionListener(new clickPollution());
		vue.getPopulation().addActionListener(new clickPopulation());
		vue.getSuperficie().addActionListener(new clickSuperficie());
	}


	private class clickPIB implements ActionListener {
		@SuppressWarnings("unused")
		public void actionPerformed(ActionEvent e) {
			try {
				partieLocale.changeTheme("cardline","pib");
			} catch (IOException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}
			try {
				partieLocale.trietapis();
				((Window) vue.getPane().getTopLevelAncestor()).dispose();
				ControleurJeu a=new ControleurJeu(new VueJeu(), partieLocale, posjoueur, listGagnants);
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
		}
	}
	
	class clickPopulation implements ActionListener {
		@SuppressWarnings("unused")
		public void actionPerformed(ActionEvent e) {
			try {
				partieLocale.changeTheme("cardline","population");
			} catch (IOException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}
			try {
				partieLocale.trietapis();
				((Window) vue.getPane().getTopLevelAncestor()).dispose();
				ControleurJeu a=new ControleurJeu(new VueJeu(), partieLocale, posjoueur, listGagnants);
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
 
		}
	}
	
	class clickPollution implements ActionListener {
		@SuppressWarnings("unused")
		public void actionPerformed(ActionEvent e) {
			try {
				partieLocale.changeTheme("cardline","pollution");
			} catch (IOException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}
			try {
				partieLocale.trietapis();
				((Window) vue.getPane().getTopLevelAncestor()).dispose();
				ControleurJeu a=new ControleurJeu(new VueJeu(), partieLocale, posjoueur, listGagnants);
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
		}
	}
	
	class clickSuperficie implements ActionListener {
		@SuppressWarnings("unused")
		public void actionPerformed(ActionEvent e) {
			try {
				partieLocale.changeTheme("cardline","superficie");
			} catch (IOException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}
			try {
				partieLocale.trietapis();
				((Window) vue.getPane().getTopLevelAncestor()).dispose();
				ControleurJeu a=new ControleurJeu(new VueJeu(), partieLocale, posjoueur, listGagnants);
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

		}
	}
	
}
