package vue;

import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Insets;

import java.io.IOException;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import model.Partie;

@SuppressWarnings("serial")
public class VueJeu extends JFrame {
	private JPanel paneJoueur = new JPanel();
	private JPanel paneTapis = new JPanel();
	private JPanel paneAdversaire = new JPanel();
	private Partie partieLocal = new Partie();
	private JLabel joueur = new JLabel();
	private int posJoueur = 0;
	private int choixMain = -1;
	private int choixTapis = -1;
	private boolean sortir = false;
	private JButton bonusAppel = new JButton("appel un amis");
	private JLabel pseudo = new JLabel();
	private JLabel temps = new JLabel("temps");
	private final ImageIcon imageIconTapis = new ImageIcon("data/img/tapis.png");
	private final ImageIcon imageIconJoueur = new ImageIcon("data/img/tapisjoueur.png");
	private GridLayout grillePaneAdversaire = new GridLayout(2, 1);
	private JPanel paneGagnants=new JPanel();
	private JPanel paneAutreJoueur=new JPanel();
	private JTextArea textAutreJoueur=new JTextArea();
	private JButton changeTheme= new JButton("changer thême");
	private JLabel infoJeu=new JLabel();

	@SuppressWarnings("static-access")
	public VueJeu() throws IOException {
		paneTapis = new JPanel() {
			Image image = imageIconTapis.getImage().getScaledInstance(4000, 400, Image.SCALE_DEFAULT);
			{
				setOpaque(false);
			} 

			public void paintComponent(Graphics g) {
				g.drawImage(image, 0, 0, this);
				super.paintComponent(g);
			}
		};

		paneJoueur = new JPanel() {
			Image image = imageIconJoueur.getImage().getScaledInstance(4000, 500, Image.SCALE_DEFAULT);
			{
				setOpaque(false);
			} 

			public void paintComponent(Graphics g) {
				g.drawImage(image, 0, 0, this);
				super.paintComponent(g);
			}
		};
		

		JScrollPane scrollPane1 = new JScrollPane(paneTapis);
		JScrollPane scrollPane2 = new JScrollPane(paneJoueur);

		JPanel contentPane = new JPanel();
		GridBagLayout gridbag = new GridBagLayout();
		GridBagConstraints c = new GridBagConstraints();
		contentPane.setLayout(gridbag);
		c.fill = GridBagConstraints.HORIZONTAL;
		c.insets = new Insets(0, 5, 0, 5);

		c.weightx = 6.5;
		c.gridx = 0;
		c.gridy = 0;
		contentPane.add(temps, c);

		c.gridx = 1;
		c.gridy = 0;
		c.gridwidth = 1;
		contentPane.add(changeTheme, c);

		c.gridx = 2;
		c.gridy = 0;
		c.gridwidth = 1;
		contentPane.add(bonusAppel, c);

		c.ipady = 340; 
		c.weightx = 0.0;
		c.gridwidth = 3;
		c.gridx = 0;
		c.gridy = 1;
		contentPane.add(scrollPane1, c);

		c.ipady = 10; 
		c.weightx = 0.0;
		c.gridx = 0;
		c.gridy = 2;
		contentPane.add(pseudo, c);
		
		c.gridx = 1;
		c.gridy = 2;
		contentPane.add(infoJeu, c);

		c.ipady = 10; 
		c.weightx = 0.0;
		c.gridx = 2;
		c.gridy = 2;
		contentPane.add(new JLabel("Joueurs restants"), c);

		c.ipady = 300;
		c.weightx = 0.0;
		c.gridwidth = 2;
		c.gridx = 0;
		c.gridy = 3;
		contentPane.add(scrollPane2, c);

		c.ipady = 180; 
		c.weightx = 0.0;
		c.gridx = 2;
		c.gridy = 3;
		contentPane.add(textAutreJoueur, c);

		this.pack();
		this.setTitle("Jeu");
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.add(contentPane);
		this.setLocationRelativeTo(null);
		this.setExtendedState(this.MAXIMIZED_BOTH);
		this.setResizable(false);
		this.setVisible(true);

	}
	


	public JButton getBonusAppel() {
		return bonusAppel;
	}

	public void setBonusAppel(JButton bonusAppel) {
		this.bonusAppel = bonusAppel;
	}

	public JPanel getPaneJoueur() {
		return paneJoueur;
	}

	public void setPaneJoueur(JPanel paneJoueur) {
		this.paneJoueur = paneJoueur;
	}

	public JPanel getPaneTapis() {
		return paneTapis;
	}

	public void setPaneTapis(JPanel paneTapis) {
		this.paneTapis = paneTapis;
	}

	public Partie getPartieLocal() {
		return partieLocal;
	}

	public void setPartieLocal(Partie partieLocal) {
		this.partieLocal = partieLocal;
	}

	public int getPosJoueur() {
		return posJoueur;
	}

	public void setPosJoueur(int posJoueur) {
		this.posJoueur = posJoueur;
	}

	public int getChoixMain() {
		return choixMain;
	}

	public void setChoixMain(int choixMain) {
		this.choixMain = choixMain;
	}

	public int getChoixTapis() {
		return choixTapis;
	}

	public void setChoixTapis(int choixTapis) {
		this.choixTapis = choixTapis;
	}

	public boolean isSortir() {
		return sortir;
	}

	public void setSortir(boolean sortir) {
		this.sortir = sortir;
	}

	public JLabel getJoueur() {
		return joueur;
	}

	public void setJoueur(JLabel joueur) {
		this.joueur = joueur;
	}

	public JPanel getPaneAdversaire() {
		return paneAdversaire;
	}

	public void setPaneAdversaire(JPanel paneAdversaire) {
		this.paneAdversaire = paneAdversaire;
	}

	public JLabel getPseudo() {
		return pseudo;
	}

	public void setPseudo(JLabel pseudo) {
		this.pseudo = pseudo;
	}

	public JLabel getTemps() {
		return temps;
	}

	public void setTemps(JLabel temps) {
		this.temps = temps;
	}

	public ImageIcon getImageIconTapis() {
		return imageIconTapis;
	}

	public GridLayout getGrillePaneAdversaire() {
		return grillePaneAdversaire;
	}

	public void setGrillePaneAdversaire(GridLayout grillePaneAdversaire) {
		this.grillePaneAdversaire = grillePaneAdversaire;
	}

	public JPanel getPaneGagnants() {
		return paneGagnants;
	}

	public void setPaneGagnants(JPanel paneGagnants) {
		this.paneGagnants = paneGagnants;
	}

	public JPanel getPaneAutreJoueur() {
		return paneAutreJoueur;
	}

	public void setPaneAutreJoueur(JPanel paneAutreJoueur) {
		this.paneAutreJoueur = paneAutreJoueur;
	}

	public ImageIcon getImageIconJoueur() {
		return imageIconJoueur;
	}

	public JTextArea getTextAutreJoueur() {
		return textAutreJoueur;
	}

	public void setTextAutreJoueur(JTextArea textAutreJoueur) {
		this.textAutreJoueur = textAutreJoueur;
	}

	public JButton getChangeTheme() {
		return changeTheme;
	}
	
	public void setChangeTheme(JButton changeTheme) {
		this.changeTheme = changeTheme;
	}

	public JLabel getInfoJeu() {
		return infoJeu;
	}



	public void setInfoJeu(JLabel infoJeu) {
		this.infoJeu = infoJeu;
	}

}
