package vue;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;

@SuppressWarnings("serial")
public class VueGagnants extends JFrame {
	private JPanel paneAction = new JPanel();
	private JPanel paneBtn = new JPanel();
	private JPanel paneClassement = new JPanel();
	private JButton recommencer=new JButton("recommencer");
	private JButton menu=new JButton("retour menu");
	private JLabel lblgagnant=new JLabel();
	private GridLayout grilleGagnant = new GridLayout(2, 1);
	private DefaultTableModel model;
	private JTable table;
	private JScrollPane pane;
	private ListSelectionModel cellSelectionModel;

	

	

	public VueGagnants() {
		this.setTitle("Classement");
		this.pack();
		
		GridBagLayout gridbag = new GridBagLayout();
		GridBagConstraints c = new GridBagConstraints();
		this.setLayout(gridbag);
		
		
		c.fill = GridBagConstraints.CENTER; 
		c.weightx = 10;
		c.gridx = 0;
		c.gridy = 0;
		c.gridwidth = 2;
		this.add(lblgagnant,c);

		c.fill = GridBagConstraints.HORIZONTAL; 
		c.ipady = 150;      //make this component tall
		c.weightx = 0.0;
		c.gridwidth = 2;
		c.gridx = 0;
		c.gridy = 1;
		this.add(paneClassement,c);
		
		c.ipady = 5;      //make this component tall
		c.weightx = 0.0;
		c.gridx = 0;
		c.gridy = 2;
		this.add(paneBtn,c);

		paneBtn.add(recommencer);
		paneBtn.add(menu);
		
		afficheClassement();

		this.setSize(450, 280);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLocationRelativeTo(null);
		this.setResizable(false);
		this.setVisible(true);
	}
	
	public void afficheClassement() {
		model = new DefaultTableModel();
		table = new JTable(model);
		
		table.setSize(new Dimension(30, 40));
		pane = new JScrollPane(table);
		model.addColumn("Pseudo");
		model.addColumn("Point");
		paneClassement.add(pane);
	}

	

	public DefaultTableModel getModel() {
		return model;
	}

	public void setModel(DefaultTableModel model) {
		this.model = model;
	}

	public JPanel getPaneClassement() {
		return paneClassement;
	}

	public void setPaneClassement(JPanel paneClassement) {
		this.paneClassement = paneClassement;
	}

	public JButton getRecommencer() {
		return recommencer;
	}

	public void setRecommencer(JButton recommencer) {
		this.recommencer = recommencer;
	}
	
	public JButton getMenu() {
		return menu;
	}

	public void setMenu(JButton menu) {
		this.menu = menu;
	}
	
	public JLabel getLblgagnant() {
		return lblgagnant;
	}

	public void setLblgagnant(JLabel lblgagnant) {
		this.lblgagnant = lblgagnant;
	}

	public JPanel getPaneAction() {
		return paneAction;
	}

	public void setPaneAction(JPanel paneAction) {
		this.paneAction = paneAction;
	}

	public GridLayout getGrilleGagnant() {
		return grilleGagnant;
	}

	public void setGrilleGagnant(GridLayout grilleGagnant) {
		this.grilleGagnant = grilleGagnant;
	}

	public JTable getTable() {
		return table;
	}

	public void setTable(JTable table) {
		this.table = table;
	}

	public JScrollPane getPane() {
		return pane;
	}

	public void setPane(JScrollPane pane) {
		this.pane = pane;
	}

	public ListSelectionModel getCellSelectionModel() {
		return cellSelectionModel;
	}

	public void setCellSelectionModel(ListSelectionModel cellSelectionModel) {
		this.cellSelectionModel = cellSelectionModel;
	}

}
