package vue;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Image;
import java.util.Properties;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;
import org.jdatepicker.impl.JDatePanelImpl;
import org.jdatepicker.impl.JDatePickerImpl;
import org.jdatepicker.impl.UtilDateModel;
import model.DateLabelFormatter;

@SuppressWarnings("serial")
public class VueMenu extends JFrame {

	private JButton cardLine = new JButton("cardLine");
	private JButton timeLine = new JButton("timeLine");
	@SuppressWarnings("unused")
	private JPanel container = new JPanel();
	private JPanel PaneJoueur = new JPanel();
	private JPanel PaneJeux = new JPanel();
	private GridLayout grilleMenu = new GridLayout(3, 1);
	private GridLayout grilleJeux = new GridLayout(1, 2);
	private DefaultTableModel model;
	private JTable table;
	private JScrollPane pane;
	private JLabel lblPseudo = new JLabel("saisir votre pseudo: ");
	private JLabel lblAge = new JLabel("date d'anniversaire: ");
	private JTextField pseudo = new JTextField();
	private JButton btnSupprimer = new JButton("supprimer");
	private JButton btnAnnuler = new JButton("annuler");
	private JButton btnAjouter = new JButton("ajouter");
	private GridLayout grilleDonneeJoueur = new GridLayout(3, 1);
	private JPanel PaneSaisieJoueur = new JPanel();
	private JPanel PaneAfficheJoueur = new JPanel();
	private JPanel PaneSupprimJoueur = new JPanel();
	private JPanel panePseudo = new JPanel();
	private JPanel paneNiveauRobot = new JPanel();
	private ListSelectionModel cellSelectionModel;
	private JCheckBox choixNiveauAucun = new JCheckBox("Aucun");
	private JCheckBox choixNiveauFacile = new JCheckBox("Facile");
	private JCheckBox choixNiveauDificile = new JCheckBox("Difficile");
	private JCheckBox choixNiveauMoyen = new JCheckBox("Moyen");
	private VueMenuBar menuBarHaut= new VueMenuBar();
	private JDatePickerImpl datePicker = new JDatePickerImpl(new JDatePanelImpl(new UtilDateModel(), new Properties()),new DateLabelFormatter());
	private JLabel ajoutJoueur=new JLabel();
	
	@SuppressWarnings({ "static-access", "unused" })
	public VueMenu() {
		this.setTitle("Jeu");
		this.pack();
		this.setDefaultLookAndFeelDecorated(true);
		this.setSize(470, 550);
		JOptionPane jop2 = new JOptionPane();
		this.setResizable(false);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLocationRelativeTo(null);
		this.setJMenuBar(menuBarHaut);
		PaneJoueur();
		PaneJeux();
		paneNiveauRobot.setVisible(false);
		this.setLayout(this.grilleMenu);
		this.add(PaneJoueur);
		this.add(PaneAfficheJoueur);
		this.add(PaneJeux);
		this.setVisible(true);
	}

	public JPanel getPaneJoueur() {
		return PaneJoueur;
	}

	public void setPaneJoueur(JPanel paneJoueur) {
		PaneJoueur = paneJoueur;
	}

	public VueMenuBar getMenuBarHaut() {
		return menuBarHaut;
	}

	public void setMenuBarHaut(VueMenuBar menuBarTest) {
		this.menuBarHaut = menuBarTest;
	}

	public void PaneJoueur() {
		PaneJoueur.setLayout(grilleDonneeJoueur);

		PaneSaisie();
		afficheJoueur();
		PaneChoixBot();
		supprimeJoueur();

		PaneJoueur.add(PaneSaisieJoueur);
		PaneJoueur.add(PaneSupprimJoueur);
		PaneJoueur.add(paneNiveauRobot);
	}

	private void supprimeJoueur() {
		btnSupprimer.setVisible(false);
		PaneSupprimJoueur.add(btnSupprimer);
		PaneSupprimJoueur.add(btnAnnuler);
		PaneSupprimJoueur.add(ajoutJoueur);

	}

	public JButton getCardLine() {
		return cardLine;
	}

	public void setCardLine(JButton cardLine) {
		this.cardLine = cardLine;
	}

	public JButton getBtnSupprimer() {
		return btnSupprimer;
	}

	public void setBtnSupprimer(JButton btnSupprimer) {
		this.btnSupprimer = btnSupprimer;
	}

	public JButton getBtnAnnuler() {
		return btnAnnuler;
	}

	public void setBtnAnnuler(JButton btnAnnuler) {
		this.btnAnnuler = btnAnnuler;
	}

	public JButton getBtnAjouter() {
		return btnAjouter;
	}

	public void setBtnAjouter(JButton btnAjouter) {
		this.btnAjouter = btnAjouter;
	}

	private void afficheJoueur() {

		model = new DefaultTableModel();
		table = new JTable(model);
		pane = new JScrollPane(table);
		model.addColumn("Pseudo");
		model.addColumn("Anniversaire");
		model.addColumn("Point");

		PaneAfficheJoueur.add(pane);

		cellSelectionModel = table.getSelectionModel();
		cellSelectionModel.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
	}

	public JTable getTable() {
		return table;
	}

	public void setTable(JTable table) {
		this.table = table;
	}

	public DefaultTableModel getModel() {
		return model;
	}

	public void setModel(DefaultTableModel model) {
		this.model = model;
	}

	public JButton getTimeLine() {
		return timeLine;
	}

	public void setTimeLine(JButton timeLine) {
		this.timeLine = timeLine;
	}

	public JTextField getPseudo() {
		return pseudo;
	}

	public void setPseudo(JTextField pseudo) {
		this.pseudo = pseudo;
	}

	public ListSelectionModel getCellSelectionModel() {
		return cellSelectionModel;
	}

	public void setCellSelectionModel(ListSelectionModel cellSelectionModel) {
		this.cellSelectionModel = cellSelectionModel;
	}

	public void PaneJeux() {
		PaneJeux.setLayout(grilleJeux);
		PaneJeux.add(cardLine);
		PaneJeux.add(timeLine);
		
		ImageIcon imageIcon = new ImageIcon("data/img/cardline.jpg");
		Image image = imageIcon.getImage();
		Image newimg = image.getScaledInstance(300, 170, java.awt.Image.SCALE_SMOOTH);
		imageIcon = new ImageIcon(newimg);
		cardLine.setIcon(imageIcon);
		
		imageIcon = new ImageIcon("data/img/timeLine.jpg");
		image = imageIcon.getImage();
		newimg = image.getScaledInstance(300, 170, java.awt.Image.SCALE_SMOOTH);
		imageIcon = new ImageIcon(newimg);
		timeLine.setIcon(imageIcon);
	}

	public void PaneSaisie() {

		pseudo.setPreferredSize(new Dimension(100, 40));
		
		datePicker.setPreferredSize(new Dimension(150, 25));
		GridLayout grilleSaisieJoueur = new GridLayout(1, 3);
		PaneSaisieJoueur.setLayout(grilleSaisieJoueur);

		panePseudo.add(lblPseudo);
		panePseudo.add(pseudo);

		JPanel paneAge = new JPanel();
		paneAge.add(lblAge);
		paneAge.add(datePicker);

		PaneSaisieJoueur.add(panePseudo);
		PaneSaisieJoueur.add(paneAge);
		PaneSaisieJoueur.add(btnAjouter);

	}

	public void PaneChoixBot() {
		JLabel lbChoixNiveau = new JLabel("niveau bot :");
		paneNiveauRobot.add(lbChoixNiveau);
		paneNiveauRobot.add(choixNiveauAucun);
		paneNiveauRobot.add(choixNiveauFacile);
		paneNiveauRobot.add(choixNiveauMoyen);
		paneNiveauRobot.add(choixNiveauDificile);

	}

	public JCheckBox getChoixNiveauAucun() {
		return choixNiveauAucun;
	}

	public void setChoixNiveauAucun(JCheckBox choixNiveauAucun) {
		this.choixNiveauAucun = choixNiveauAucun;
	}

	public JCheckBox getChoixNiveauFacile() {
		return choixNiveauFacile;
	}

	public void setChoixNiveauFacile(JCheckBox choixNiveauFacile) {
		this.choixNiveauFacile = choixNiveauFacile;
	}

	public JCheckBox getChoixNiveauDificile() {
		return choixNiveauDificile;
	}

	public void setChoixNiveauDificile(JCheckBox choixNiveauDificile) {
		this.choixNiveauDificile = choixNiveauDificile;
	}

	public JCheckBox getChoixNiveauMoyen() {
		return choixNiveauMoyen;
	}

	public void setChoixNiveauMoyen(JCheckBox choixNiveauMoyen) {
		this.choixNiveauMoyen = choixNiveauMoyen;
	}

	public JPanel getPaneNiveauRobot() {
		return paneNiveauRobot;
	}

	public void setPaneNiveauRobot(JPanel paneNiveauRobot) {
		this.paneNiveauRobot = paneNiveauRobot;
	}

	public JDatePickerImpl getDatePicker() {
		return datePicker;
	}

	public void setDatePicker(JDatePickerImpl datePicker) {
		this.datePicker = datePicker;
	}

	public JLabel getAjoutJoueur() {
		return ajoutJoueur;
	}

	public void setAjoutJoueur(JLabel ajoutJoueur) {
		this.ajoutJoueur = ajoutJoueur;
	}

}
