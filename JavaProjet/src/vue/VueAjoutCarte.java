package vue;


import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;



public class VueAjoutCarte extends JFrame{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JButton chercherCarteCardLineCache=new JButton("Image cache");
	private JButton chercherCarteCardLineOuverte=new JButton("Image ouverte");
	private JButton chercherCarteTimelineCache=new JButton("Image cache");
	private JButton chercherCarteTimelineOuvert=new JButton("Image ouverte");
	private JButton ajouterCardline=new JButton("ajouter");
	private JButton ajouterTimeLine=new JButton("ajouter");
	private JButton imageCardlineOuvert=new JButton();
	private JButton imageCardlineCache=new JButton();
	private JButton imageTimelineOuvert=new JButton();
	private JButton imageTimelineCache=new JButton();
	private JButton retourMenu1=new JButton("Retour menu");
	private JButton retourMenu2=new JButton("Retour menu");
	

	private JTabbedPane tabbedPane;
    JTabbedPane tabs=new JTabbedPane(JTabbedPane.TOP, JTabbedPane.WRAP_TAB_LAYOUT);;
    cardlinePanel cardline=new cardlinePanel();
    timelinePanel timeline=new timelinePanel();
    
    private JLabel lblInvention;
    private JLabel lblSuperficie;
	private JLabel lblPopulation=new JLabel();
	private JLabel lblPib=new JLabel();
	private JLabel lblPollution=new JLabel("");
	private JLabel lblDate=new JLabel("date: ");
	private JLabel lblVille;
	
	
	
	
	private int x=190;
	private int y=30;
	


	public VueAjoutCarte() {
		this.setTitle("Ajout Carte");
		this.pack();
		this.setSize(550, 330);
		this.setResizable(false);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLocationRelativeTo(null);	
		tabs.addTab("TimeLine", timeline);
        tabs.addTab("CardLine", cardline);
        getContentPane().add(tabs);
		this.setVisible(true);
				
	}
	
	public class cardlinePanel extends JPanel {
		
	    /**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		private JTextField superficie=new JTextField();
		private JTextField population=new JTextField();
		private JTextField pib=new JTextField();
		private JTextField pollution=new JTextField();
		private JTextField ville=new JTextField();
		
		
		private JLabel lblValidationCardLine=new JLabel();
		

		cardlinePanel() {
			
			superficie.setMinimumSize(new Dimension(x, y));
			 population.setMinimumSize(new Dimension(x, y));
			 pib.setMinimumSize(new Dimension(x, y));
			 pollution.setMinimumSize(new Dimension(x, y));
			 ville.setMinimumSize(new Dimension(x, y));
			
			
			/* On ajoute un gridbagLauout au panel */
			setLayout(new GridBagLayout());
			
			/* Le gridBagConstraints va définir la position et la taille des éléments */
			GridBagConstraints gc = new GridBagConstraints();
			

			gc.fill = GridBagConstraints.FIRST_LINE_START;
						
			/* ipady permet de savoir où on place le composant s'il n'occupe pas la totalité de l'espace disponnible */
			gc.ipady = gc.anchor = GridBagConstraints.EAST;

			/* weightx définit le nombre de cases en abscisse */
			gc.weightx = 4;
			
			/* weightx définit le nombre de cases en ordonnée */
			gc.weighty = 4;
			
			gc.gridx = 0;
			gc.gridy = 0;
			add(new JLabel("PIB: "), gc);
			gc.fill = GridBagConstraints.HORIZONTAL;
			gc.gridx = 1;
			gc.gridy = 0;
			add(pib, gc);
			
			gc.fill = GridBagConstraints.EAST;
			gc.gridx = 2;
			gc.gridy = 0;
			add(new JLabel("superficie: "), gc);
			
			gc.fill = GridBagConstraints.HORIZONTAL;
			gc.gridx = 3;
			gc.gridy = 0;
			add(superficie, gc);
			
			
			gc.fill = GridBagConstraints.EAST;
			gc.gridx = 0;
			gc.gridy = 1;
			add(new JLabel("Pollution: "), gc);
			
			gc.fill = GridBagConstraints.HORIZONTAL;
			gc.gridx = 1;
			gc.gridy = 1;
			add(pollution, gc);
			
			gc.fill = GridBagConstraints.EAST;
			gc.gridx = 2;
			gc.gridy = 1;
			add(new JLabel("Population: "), gc);
			
			gc.fill = GridBagConstraints.HORIZONTAL;
			gc.gridx = 3;
			gc.gridy = 1;
			add(population, gc);
			
			gc.fill = GridBagConstraints.EAST;
			gc.gridx = 0;
			gc.gridy = 2;
			add(new JLabel("Ville: "), gc);
			
			gc.fill = GridBagConstraints.HORIZONTAL;
			gc.gridx = 1;
			gc.gridy = 2;
			add(ville, gc);
			
			gc.fill = GridBagConstraints.EAST;
			gc.gridx = 0;
			gc.gridy = 3;
			add(chercherCarteCardLineOuverte, gc);
			
			gc.fill = GridBagConstraints.HORIZONTAL;
			gc.gridx = 1;
			gc.gridy = 3;
			add(imageCardlineOuvert, gc);
			
			gc.fill = GridBagConstraints.EAST;
			gc.gridx = 2;
			gc.gridy = 3;
			add(chercherCarteCardLineCache, gc);
			
			gc.fill = GridBagConstraints.HORIZONTAL;
			gc.gridx = 3;
			gc.gridy = 3;
			add(imageCardlineCache, gc);
			
			gc.fill = GridBagConstraints.EAST;
			gc.gridx = 0;
			gc.gridy = 4;
			add(ajouterCardline, gc);
			
			gc.fill = GridBagConstraints.EAST;
			gc.gridx = 1;
			gc.gridy = 4;
			add(lblValidationCardLine, gc);
			
			gc.fill = GridBagConstraints.EAST;
			gc.gridx = 3;
			gc.gridy = 4;
			add(retourMenu1, gc);
			
	    	
	    }

		public JTextField getSuperficie() {
			return superficie;
		}

		public void setSuperficie(JTextField superficie) {
			this.superficie = superficie;
		}

		public JTextField getPopulation() {
			return population;
		}

		public void setPopulation(JTextField population) {
			this.population = population;
		}

		public JTextField getPib() {
			return pib;
		}

		public void setPib(JTextField pib) {
			this.pib = pib;
		}

		public JTextField getPollution() {
			return pollution;
		}

		public void setPollution(JTextField pollution) {
			this.pollution = pollution;
		}

		public JTextField getVille() {
			return ville;
		}

		public void setVille(JTextField ville) {
			this.ville = ville;
		}

		public JLabel getLblValidationCardLine() {
			return lblValidationCardLine;
		}

		public void setLblValidationCardLine(JLabel lblValidationCardLine) {
			this.lblValidationCardLine = lblValidationCardLine;
		}
	}
	
	
	public class timelinePanel extends JPanel {
	    /**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		private JTextField invention=new JTextField();
		private JTextField date=new JTextField();
		private JLabel lblValidationTimeLine=new JLabel();
		
		timelinePanel() {
			
			/* On ajoute un gridbagLauout au panel */
			setLayout(new GridBagLayout());
			
			/* Le gridBagConstraints va définir la position et la taille des éléments */
			GridBagConstraints gc = new GridBagConstraints();

			gc.fill = GridBagConstraints.FIRST_LINE_START;
			
			/* ipady permet de savoir où on place le composant s'il n'occupe pas la totalité de l'espace disponnible */
			gc.ipady = gc.anchor = GridBagConstraints.EAST;

			/* weightx définit le nombre de cases en abscisse */
			gc.weightx = 2;
			
			/* weightx définit le nombre de cases en ordonnée */
			gc.weighty = 3;
			
			gc.gridx = 0;
			gc.gridy = 0;
			add(new JLabel("Invention: "), gc);
			gc.fill = GridBagConstraints.HORIZONTAL;
			gc.gridx = 1;
			gc.gridy = 0;
			add(invention, gc);
			
			
			gc.fill = GridBagConstraints.EAST;
			gc.gridx = 0;
			gc.gridy = 1;
			add(new JLabel("Date: : "), gc);
			
			gc.fill = GridBagConstraints.HORIZONTAL;
			gc.gridx = 1;
			gc.gridy = 1;
			add(date, gc);
			
			gc.fill = GridBagConstraints.EAST;
			gc.gridx = 0;
			gc.gridy = 2;
			add(chercherCarteTimelineOuvert, gc);
			
			gc.fill = GridBagConstraints.HORIZONTAL;
			gc.gridx = 1;
			gc.gridy = 2;
			add(imageTimelineOuvert, gc);
			
			gc.fill = GridBagConstraints.EAST;
			gc.gridx = 0;
			gc.gridy = 3;
			add(chercherCarteTimelineCache, gc);
			
			gc.fill = GridBagConstraints.HORIZONTAL;
			gc.gridx = 1;
			gc.gridy = 3;
			add(imageTimelineCache, gc);
			
			gc.fill = GridBagConstraints.EAST;
			gc.gridx = 0;
			gc.gridy = 4;
			add(ajouterTimeLine, gc);
			
			
			gc.fill = GridBagConstraints.EAST;
			gc.gridx = 1;
			gc.gridy = 4;
			add(retourMenu2, gc);
			
			gc.fill = GridBagConstraints.EAST;
			gc.gridx = 0;
			gc.gridy = 5;
			add(lblValidationTimeLine, gc);
			
	    }

		public JTextField getInvention() {
			return invention;
		}

		public void setInvention(JTextField invention) {
			this.invention = invention;
		}

		public JTextField getDate() {
			return date;
		}

		public void setDate(JTextField date) {
			this.date = date;
		}

		public JLabel getLblValidationTimeLine() {
			return lblValidationTimeLine;
		}

		public void setLblValidationTimeLine(JLabel lblValidationTimeLine) {
			this.lblValidationTimeLine = lblValidationTimeLine;
		}

	}
	

	public JButton getAjouterCardline() {
		return ajouterCardline;
	}

	public void setAjouterCardline(JButton ajouterCardline) {
		this.ajouterCardline = ajouterCardline;
	}

	public JButton getAjouterTimeLine() {
		return ajouterTimeLine;
	}

	public void setAjouterTimeLine(JButton ajouterTimeLine) {
		this.ajouterTimeLine = ajouterTimeLine;
	}


	public JTabbedPane getTabbedPane() {
		return tabbedPane;
	}

	public void setTabbedPane(JTabbedPane tabbedPane) {
		this.tabbedPane = tabbedPane;
	}

	public JTabbedPane getTabs() {
		return tabs;
	}

	public void setTabs(JTabbedPane tabs) {
		this.tabs = tabs;
	}

	public cardlinePanel getCardline() {
		return cardline;
	}

	public void setCardline(cardlinePanel cardline) {
		this.cardline = cardline;
	}

	public timelinePanel getTimeline() {
		return timeline;
	}

	public void setTimeline(timelinePanel timeline) {
		this.timeline = timeline;
	}

	public JButton getChercherCarteCardLineCache() {
		return chercherCarteCardLineCache;
	}

	public void setChercherCarteCardLineCache(JButton chercherCarteCardLineCache) {
		this.chercherCarteCardLineCache = chercherCarteCardLineCache;
	}

	public JButton getChercherCarteCardLineOuverte() {
		return chercherCarteCardLineOuverte;
	}

	public void setChercherCarteCardLineOuverte(JButton chercherCarteCardLineOuverte) {
		this.chercherCarteCardLineOuverte = chercherCarteCardLineOuverte;
	}

	public JButton getChercherCarteTimelineCache() {
		return chercherCarteTimelineCache;
	}

	public void setChercherCarteTimelineCache(JButton chercherCarteTimelineCache) {
		this.chercherCarteTimelineCache = chercherCarteTimelineCache;
	}

	public JButton getChercherCarteTimelineOuvert() {
		return chercherCarteTimelineOuvert;
	}

	public void setChercherCarteTimelineOuvert(JButton chercherCarteTimelineOuvert) {
		this.chercherCarteTimelineOuvert = chercherCarteTimelineOuvert;
	}

	public JButton getImageCardlineOuvert() {
		return imageCardlineOuvert;
	}

	public void setImageCardlineOuvert(JButton imageCardlineOuvert) {
		this.imageCardlineOuvert = imageCardlineOuvert;
	}

	public JButton getImageCardlineCache() {
		return imageCardlineCache;
	}

	public void setImageCardlineCache(JButton imageCardlineCache) {
		this.imageCardlineCache = imageCardlineCache;
	}

	public JButton getImageTimelineOuvert() {
		return imageTimelineOuvert;
	}

	public void setImageTimelineOuvert(JButton imageTimelineOuvert) {
		this.imageTimelineOuvert = imageTimelineOuvert;
	}

	public JButton getImageTimelineCache() {
		return imageTimelineCache;
	}

	public void setImageTimelineCache(JButton imageTimelineCache) {
		this.imageTimelineCache = imageTimelineCache;
	}


	public JLabel getLblSuperficie() {
		return lblSuperficie;
	}

	public void setLblSuperficie(JLabel lblSuperficie) {
		this.lblSuperficie = lblSuperficie;
	}

	public JLabel getLblPopulation() {
		return lblPopulation;
	}

	public void setLblPopulation(JLabel lblPopulation) {
		this.lblPopulation = lblPopulation;
	}

	public JLabel getLblPib() {
		return lblPib;
	}

	public void setLblPib(JLabel lblPib) {
		this.lblPib = lblPib;
	}

	public JLabel getLblPollution() {
		return lblPollution;
	}

	public void setLblPollution(JLabel lblPollution) {
		this.lblPollution = lblPollution;
	}

	public JLabel getLblDate() {
		return lblDate;
	}

	public void setLblDate(JLabel lblDate) {
		this.lblDate = lblDate;
	}

	public JLabel getLblInvention() {
		return lblInvention;
	}

	public void setLblInvention(JLabel lblInvention) {
		this.lblInvention = lblInvention;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public JLabel getLblVille() {
		return lblVille;
	}

	public void setLblVille(JLabel lblVille) {
		this.lblVille = lblVille;
	}

	public JButton getRetourMenu1() {
		return retourMenu1;
	}

	public void setRetourMenu1(JButton retourMenu1) {
		this.retourMenu1 = retourMenu1;
	}

	public JButton getRetourMenu2() {
		return retourMenu2;
	}

	public void setRetourMenu2(JButton retourMenu2) {
		this.retourMenu2 = retourMenu2;
	}


	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

}
