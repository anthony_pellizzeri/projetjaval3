package vue;

import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;

import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.KeyStroke;

public class VueMenuBar extends JMenuBar {
	private static final long serialVersionUID = -2646606467969487514L;
	private JMenu menuFichier;
	private JMenu menuOption;
	private JMenuItem menuItemEnregistrer;
	private JMenuItem menuItemOuvrir;
	private JMenuItem menuItemAide;
	private JMenuItem menuItemAjoutCarte;
	private JMenuItem menuItemMusique;
	



	public VueMenuBar() {
		this.menuFichier = new JMenu("Fichier");
		this.menuOption = new JMenu("Option");
		this.menuItemEnregistrer = new JMenuItem("Enregister");
		this.menuItemOuvrir = new JMenuItem("Ouvrir");
		this.menuItemAjoutCarte = new JMenuItem("Ajout carte");
		this.menuItemMusique = new JMenuItem("Musique");
		this.menuItemAide = new JMenuItem("Aide");
		this.add(menuFichier);
		this.menuItemEnregistrer.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.META_DOWN_MASK));
		this.menuItemOuvrir.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, InputEvent.META_DOWN_MASK));
		this.menuItemAide.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_H, InputEvent.META_DOWN_MASK));
		this.menuItemAjoutCarte.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_B, InputEvent.META_DOWN_MASK));
		this.menuItemMusique.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_M, InputEvent.META_DOWN_MASK));
		this.menuFichier.add(menuItemEnregistrer);
		this.menuFichier.add(menuItemOuvrir);
		this.add(menuFichier);
		this.menuOption.add(menuItemAjoutCarte);
		this.menuOption.add(menuItemMusique);
		this.menuOption.add(menuItemAide);
		this.add(menuOption);
	}

	public JMenu getMenuFichier() {
		return menuFichier;
	}

	public void setMenuFichier(JMenu menuFichier) {
		this.menuFichier = menuFichier;
	}

	public JMenuItem getMenuItemEnregistrer() {
		return menuItemEnregistrer;
	}

	public void setMenuItemEnregistrer(JMenuItem menuItemEnregistrer) {
		this.menuItemEnregistrer = menuItemEnregistrer;
	}

	public JMenuItem getMenuItemOuvrir() {
		return menuItemOuvrir;
	}

	public void setMenuItemOuvrir(JMenuItem menuItemOuvrir) {
		this.menuItemOuvrir = menuItemOuvrir;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public JMenu getMenuOption() {
		return menuOption;
	}

	public void setMenuOption(JMenu menuOption) {
		this.menuOption = menuOption;
	}

	public JMenuItem getMenuItemAide() {
		return menuItemAide;
	}

	public void setMenuItemAide(JMenuItem menuItemAide) {
		this.menuItemAide = menuItemAide;
	}
	
	public JMenuItem getMenuItemAjoutCarte() {
		return menuItemAjoutCarte;
	}

	public void setMenuItemAjoutCarte(JMenuItem menuItemAjoutCarte) {
		this.menuItemAjoutCarte = menuItemAjoutCarte;
	}

	public JMenuItem getMenuItemMusique() {
		return menuItemMusique;
	}

	public void setMenuItemMusique(JMenuItem menuItemMusique) {
		this.menuItemMusique = menuItemMusique;
	}

}