package vue;

import java.io.IOException;
import controleur.ControleurMenu;
import model.specification.*;
import model.Partie;

public class Affichage {
	
	/**
	 * lance le jeu
	 * @author anthonypellizzeri abdel 
	 * @param args
	 * @throws IOException
	 */

	@SuppressWarnings("unused")
	public static void main(String[] args) throws IOException {
		Partie partie = new Partie();
		partie.setTypeJeu(typeJeu.timeline);
		partie.lecture();
		ControleurMenu c = new ControleurMenu(partie, new VueMenu());
	}

}
