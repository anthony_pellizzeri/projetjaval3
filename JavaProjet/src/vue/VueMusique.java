package vue;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class VueMusique extends JFrame{
	private JButton MusiqueOn=new JButton();
	private JButton retourMenu=new JButton("Retour menu");
	private JPanel pane=new JPanel();
	
	public VueMusique() {
		this.setTitle("Musique");
		this.pack();
		this.setSize(300, 200);
		this.setResizable(false);
		this.setResizable(false);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLocationRelativeTo(null);
		
		
		pane.setLayout(new GridBagLayout());
		
		GridBagConstraints gc = new GridBagConstraints();
		
		gc.fill = GridBagConstraints.BOTH;
		
		gc.insets = new Insets(0, 0, 0, 0);
		
		gc.ipady = gc.anchor = GridBagConstraints.CENTER;

		gc.weightx = 2;
		gc.weighty = 2;
		
		
		gc.gridx = 0;
		gc.gridy = 0;
		ImageIcon imageIcon = new ImageIcon("data/img/sound.png");
		Image image = imageIcon.getImage();
		Image newimg = image.getScaledInstance(200, 200, java.awt.Image.SCALE_SMOOTH);
		imageIcon = new ImageIcon(newimg);
		MusiqueOn.setIcon(imageIcon);
		pane.add(MusiqueOn, gc);
		
		pane.add(retourMenu);
		
		

		this.setVisible(true);
		this.add(pane);
	}



	public JButton getRetourMenu() {
		return retourMenu;
	}

	public void setRetourMenu(JButton retourMenu) {
		this.retourMenu = retourMenu;
	}

	public JPanel getPane() {
		return pane;
	}

	public void setPane(JPanel pane) {
		this.pane = pane;
	}

	public JButton getMusiqueOn() {
		return MusiqueOn;
	}

	public void setMusiqueOn(JButton musiqueOn) {
		MusiqueOn = musiqueOn;
	}

}