package vue;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;


@SuppressWarnings("serial")
public class VueChoixTheme extends JFrame{
	private JButton pib=new JButton("PIB");
	private JButton pollution=new JButton("pollution");
	private JButton population=new JButton("population");
	private JButton superficie=new JButton("superficie");
	private JPanel pane=new JPanel();
	
	public VueChoixTheme() {
		this.setTitle("Choix thème");
		this.pack();
		this.setSize(500, 400);
		this.setResizable(false);
		this.setResizable(false);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLocationRelativeTo(null);
		
		
		pane.setLayout(new GridBagLayout());
		
		GridBagConstraints gc = new GridBagConstraints();
		
		gc.fill = GridBagConstraints.BOTH;
		
		gc.insets = new Insets(0, 0, 0, 0);
		
		gc.ipady = gc.anchor = GridBagConstraints.CENTER;

		gc.weightx = 2;
		gc.weighty = 2;
		
		
		gc.gridx = 0;
		gc.gridy = 0;
		ImageIcon imageIcon = new ImageIcon("data/img/PIB.jpg");
		Image image = imageIcon.getImage();
		Image newimg = image.getScaledInstance(260, 190, java.awt.Image.SCALE_SMOOTH);
		imageIcon = new ImageIcon(newimg);
		pib.setIcon(imageIcon);
		pane.add(pib, gc);
		
		gc.gridx = 1;
		gc.gridy = 0;
		imageIcon = new ImageIcon("data/img/pollution.jpg");
		image = imageIcon.getImage();
		newimg = image.getScaledInstance(270, 190, java.awt.Image.SCALE_SMOOTH);
		imageIcon = new ImageIcon(newimg);
		pollution.setIcon(imageIcon);
		pane.add(pollution, gc);
		
		gc.gridx = 0;
		gc.gridy = 1;
		imageIcon = new ImageIcon("data/img/population.jpg");
		image = imageIcon.getImage();
		newimg = image.getScaledInstance(260, 190, java.awt.Image.SCALE_SMOOTH);
		imageIcon = new ImageIcon(newimg);
		population.setIcon(imageIcon);
		pane.add(population, gc);
		
		gc.gridx = 1;
		gc.gridy = 1;
		imageIcon = new ImageIcon("data/img/superficie.jpg");
		image = imageIcon.getImage();
		newimg = image.getScaledInstance(260, 190, java.awt.Image.SCALE_SMOOTH);
		imageIcon = new ImageIcon(newimg);
		superficie.setIcon(imageIcon);
		pane.add(superficie, gc);

		this.setVisible(true);
		this.add(pane);
	}

	public JButton getPib() {
		return pib;
	}

	public void setPib(JButton pib) {
		this.pib = pib;
	}

	public JButton getPollution() {
		return pollution;
	}

	public void setPollution(JButton pollution) {
		this.pollution = pollution;
	}

	public JButton getPopulation() {
		return population;
	}

	public void setPopulation(JButton population) {
		this.population = population;
	}

	public JButton getSuperficie() {
		return superficie;
	}

	public void setSuperficie(JButton superficie) {
		this.superficie = superficie;
	}

	public JPanel getPane() {
		return pane;
	}

	public void setPane(JPanel pane) {
		this.pane = pane;
	}
}
